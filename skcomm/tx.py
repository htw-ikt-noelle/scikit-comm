""" 
.. autosummary::

    generate_bits
    mapper
    pulseshaper
    insert_frame_sync_seq
    insert_ch_est_seq
    insert_cpe_pilots

"""
import logging
import numpy as np
import scipy.signal as ssignal
import matplotlib.pyplot as plt

from . import utils
from . import filters
from . import signal

skc_log = logging.getLogger(__name__)

def generate_bits(n_bits=2**15, type='random', seed=None):
    """
    Generate an array of size (n_bits,) binary values.

    Parameters
    ----------
    n_bits : int, optional
        Number of bits to be generated. The default is 2**15.
    type : string, optional
        How should the bits be generated. 'random' generates n_bits unifomly 
        distributed bits.  The default is 'random'.
    seed : int, optional
        Seed of the random number generator. The default is None.    

    Returns
    -------
    bits : 1D numpy array, bool
        np.ndarray of shape (n_bits,) containing bools.
    """
    
    if type == 'random':
        rng = np.random.default_rng(seed=seed)
        bits = rng.integers(0, high=2, size=n_bits, dtype=bool)
    else:
        raise ValueError('type not implemented yet...')
    
    return bits



def mapper(bits, constellation):
    """ 
    Map bits to a given constellation alphabet.
	
	Bits are grouped into blocks of log2(constellation.size) and converted to
	decimals. These decimals are used to index the particular constellation 
	value in the constellation array.	
    
    Parameters
    ----------
    bits : 1D numpy array, bool
        Bits to be mapped to constallation symbols.
    constellation : 1D numpy array, complex
        Constellation (symbol) alphabet onto which the bits (or group of bits) 
        are mapped
    

    Returns
    -------
    symbols : 1D numpy array, complex
        np.ndarray of shape (n_bits/np.log2(constellation.size),) containing the
        constellation (or symbol) sequence after mapping.
    """
    
    if constellation.ndim > 1:
        raise ValueError('multiple, different constellations not allowed yet...')    
    
    if bits.ndim > 1:
        raise ValueError('number of dimensions of bits should be 1')   
        
    m = int(np.log2(constellation.size))
    
    if bits.shape[0] % m:
        raise ValueError('number of bits mus be an integer multiple of m')
    
    decimals = np.full((int(bits.shape[0]/m),), np.nan)
    
    if m == 1:
        decimals = bits        
    else:
        decimals = utils.bits_to_dec(bits, m)
    
    symbols = constellation[decimals.astype(int)]
    
    return symbols


def pulseshaper(samples, upsampling=2.0, pulseshape='rc', roll_off=0.2):
    """
    Upsample and pulseshape a given sample sequence.
    
    The provided samples are upsampled by the factor upsampling.
    
    This is done by inserting ceil(upsampling)-1 zeros between each sample followed 
    by applying a pulseshaping filter. (Root) raised cosine and rectangular filter
    impulse respnses are available. pulseshape = None does not apply any filter 
    to the upsampled sequence.
    
    After the pulseshaping a resampling (downsampling) is performed in case of a
    fractional upsampling factor.    

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        input signal.
    upsampling : float, optional
        upsampling factor. The default is 2.0
    pulseshape : sting, optional
        pulseshaping filter, can either be 'rc', 'rrc', 'rect' or 'None', 
        meaning raised cosine filter, root raised cosine filter, rectangular 
        filter or no filter, respectively. The default is 'rc'.
    roll_off : float, optional
        rolloff factor in case of (root) raised consine filter. The default is 0.2.

    
    Returns
    -------
    samples_out : 1D numpy array, real or complex
        upsampled and pulseshaped signal samples.

    """ 
    if samples.ndim > 1:
        raise ValueError('number of dimensions of samples should be 1...') 
        
    # integer upsampling factor before pulseshaping
    upsampling_int = int(np.ceil(upsampling))
        
    if upsampling%1:        
        # remaining fractional factor for downsampling after pulseshaping
        resampling_rem = upsampling / upsampling_int
    else:
        # no resampling necessary
        resampling_rem = None      
       
    if upsampling == 1:
        # shortcut
        return samples
    
    # upsampling (insert zeros between sampling points)
    # changed implementation necessary due to bug in scipy from version 1.5.0
    # samples_up = ssignal.upfirdn(np.asarray([1]), samples, up=upsampling, down=1)
    tmp = np.zeros((samples.size, upsampling_int-1))
    samples_up = np.c_[samples, tmp]
    samples_up = np.reshape(samples_up,-1)
    
    # check if symbols are real
    if np.isrealobj(samples_up):
        real = True
    else:
        real = False
    
    # actual pulseshaping filter
    if pulseshape.lower() == 'rc':
        samples_out = filters.raised_cosine_filter(samples_up, 
                                                   sample_rate=upsampling_int, 
                                                   roll_off=roll_off,
                                                   domain='freq') * upsampling_int
    elif pulseshape.lower() == 'rrc':
        samples_out = filters.raised_cosine_filter(samples_up, 
                                                   sample_rate=upsampling_int, 
                                                   roll_off=roll_off, 
                                                   root_raised=True,
                                                   domain='freq') * upsampling_int
    elif pulseshape.lower() == 'rect':
        samples_out = filters.moving_average(samples_up, upsampling_int, 
                                             domain='freq') * upsampling_int
    elif pulseshape.lower() == 'none':
        samples_out = samples_up
    else:
        raise ValueError('puseshape can only be either rc, rrc, None or rect...') 
        
    # if symbols are real, the pulseshaped samples should be real as well
    if real:
        samples_out = np.real(samples_out)
        
    if resampling_rem:
        # check for an integer number of samples after resampling
        if (samples_out.size * resampling_rem) % 1:            
            raise ValueError('Length of resampled vector must be an integer. Modify vector length or upsampling factor.')
        # resampling
        samples_out = ssignal.resample(samples_out, int(resampling_rem*len(samples_out)))
    
    return samples_out

def insert_frame_sync_seq(sig:signal.Signal, dim:int=0, n_symb:int=8, fs_header_len:int=128, mean_power:float=1.0, seed:int=10)->signal.Signal:
    """ Insert a frame sync header into the signal `sig`.

    The signal `sig` has to be sampled with a sample rate of 1 sample per symbol to insert the frame sync header.

    A header sequence for temporal frame synchronization (and carrier frequency offset estimation) is inserted into
    the specified dimension `dim` of the signal `sig`. Therefore a header with a total length of `fs_header_len` samples
    is inserted at the begining of sig.samples[dim], while the actual payload data is overwritten. The corresponding
    logical information at the begining of the signal is removed as well.

    This header constists of a symbol sequence ('B') of length `fs_header_len`/4
    which is repeated four times, while the third repetition of is multiplied by -1 as proposed in [1]. 

    Each of the 'B' sequence consists again of a repeating pattern of (at least) four shorter sub-sequences ('A'), each containing 
    `n_symb` random QPSK symbols. Depending on `n_symb`, also 8, 12,... etc. (an integer multiple of four) sequences 'A' can be
    included in one 'B' sequence (compare graph below).

    The mean power of the frame synchronization header sequence and the random number seed can be specified via the parameters
    `mean_power` and `seed`, respectively.

    All parameters to reproduce the header sequence (i.e. `fs_header_len`, `n_symb`, `fs_header`, `seed`, `mean_power`) are 
    written into the sig.info[dim]['fs_header'] dict.

    The following graph visualizes the cascaded 'A' and 'B' header sequence structure::

    
        ==============-------------------------------------
        | B           | B         | -B         | B         |
        ==============-------------------------------------
                |
              /   \\
         _____      _______________________________________________________
        /                                                                  \\
        _____________________________________________________________________
        | A     | A     | -A    | A     || A    | A     | -A    | A     | ...
        _____________________________________________________________________

    References
    ----------
    [1] Shi et al. "Coarse Frame and Carrier Synchronization of OFDM Systems: A New Metric and Comparison", IEEE Transactions on wirless communiations vol 3, no. 4 2004

    Parameters
    ----------
    sig:
        signal to insert the frame synchronization header into.
    dim:
        Dimension of the signal into which the frame synchrinization header should be inserted.
    n_symb: 
        Length  of the 'A' sequence (number of QPSK symbols included in 'A' sequence). The lengths of the 'B' sequences (`fs_header_len`/4)
        divided `n_symb` must be an integer multiple of four. That means that any 'B' sequence must contain an integer mulitple of four 'A'
        sequences (compare graph above).
    fs_header_len: 
        Length of the overall frame synchronization header (four 'B' sequences).
    mean_power: 
        The framce synchronization sequence is scaled in order to have a mean power of 'mean_power`.
    seed: 
        The `seed` is used to initialize the random number generator which generates the QPSK symbols contained in the 'A' sequences.

    Returns
    -------
        Signal with inserted frame synchronization header sequence.

        
    See Also
    --------
    :meth:`skcomm.rx.da_frame_sync()`
    :meth:`skcomm.rx.da_cfo_estimation()`
    """

    # TODO: maybe change naming of input parameters and use lengths of A and B sequences as input parameters? 

    if sig.n_dims < dim+1:
        raise ValueError('signal has not enough dimensions to insert frame sync header in requested dimension')
    
    if sig.sample_rate[dim]/sig.symbol_rate[dim] != 1:
        raise ValueError('signal has to be sampled at 1 sample per symbol to insert frame sync header')
    
    if (fs_header_len/4/n_symb) % 4:
        raise ValueError('header_len/4 divided by n_symb must be in integer multiple of 4')

    rng = np.random.default_rng(seed=seed)
    # 'A' sequence generation
    # generate n_symb random symbols ('A') sequence (QPSK symbols)
    sub_fs_header = (rng.integers(low=0, high=2, size=n_symb) - 0.5) * 2 + 1j*(rng.integers(low=0, high=2, size=n_symb) - 0.5) * 2
    # repeat four times to generate frame sync sub sequence (A,A,-A,A) --> sequence which ensures largest CFO "catch range"
    sub_fs_header = np.concatenate((sub_fs_header,sub_fs_header,-sub_fs_header,sub_fs_header))
    # 'B' sequence generation
    # repeat sub sequence to generate 1/4th of the whole frame sync sequence ('B' sequence)
    sub_seq_reps = int((fs_header_len/4) / (n_symb*4))
    sub_fs_header = np.tile(sub_fs_header, sub_seq_reps)
    # generate whole frame sequence header by repeating sub sequence with the pattern (B,B,-B,B)
    fs_header = np.concatenate((sub_fs_header,sub_fs_header,-sub_fs_header,sub_fs_header))
    # set mean power
    fs_header = fs_header * np.sqrt(mean_power/np.mean(np.abs(fs_header)**2))

    # debug info
    skc_log.debug(f'mean power of sent FS block: {np.mean(np.abs(fs_header)**2):.3f}')
    skc_log.debug(f'max. CFO to estimate with given frame sync sequence: +-{2*sig.sample_rate[dim]/(n_symb*4)/1e6:.3f} MHz')
    skc_log.debug(f'max. CFO to estimate with highest reliabtility for given frame sync sequence: +-{2*sig.sample_rate[dim]/fs_header_len/1e6:.3f} MHz')

    # insert header into signal and remove corresponding logical information
    sig.samples[dim][:fs_header_len] = fs_header
    bits_per_symb = int(np.log2(sig.constellation[dim].size))
    sig.symbols[dim] = sig.symbols[dim][fs_header_len::]
    sig.bits[dim] = sig.bits[dim][(fs_header_len*bits_per_symb)::]

    # write header info into signal
    fs_header_info = dict()
    fs_header_info['length'] = fs_header_len
    fs_header_info['n_symb'] = n_symb
    fs_header_info['header'] = fs_header
    fs_header_info['seed'] = seed
    fs_header_info['mean power'] = mean_power
    sig.info[dim]['fs_header'] = fs_header_info

    return sig

def insert_ch_est_seq(sig:signal.Signal, dim:int=0, offset:int=0, n_fft:int=256, n_pilots:int=64, pilot_offset:int=0, 
                      n_repetitions:int=16, mean_power:float=1.0, seed:int=11)->signal.Signal:
    """ Insert a channel estimation header into the signal `sig`.

    The signal `sig` has to be sampled with a sample rate of 1 sample per symbol to insert the frame sync header.

    An OFDM symbol consisting of `n_fft` samples (frequency bins), which is repeated `n_repetitions` times for noise averaging,
    is used as a channel estimation header sequence. Out of the `n_fft` frequency bins, `n_pilots` evenly spaced bins are filled
    with random QPSK modulation symbols, while the other frequency bins are set to zero (and could therefore be used for channel estimation
    from other MIMO channels). The random number seed of the random number generator to generate the QPSK symbols can be
    specified by `seed`.
    
    The location of the `n_pilots` QPSK modulated frequency bins can be specified with `pilot_offset`: 
    The first pilot starts at frequency bin `pilot_offset` and the pilots are inserted equally spaced `n_fft`/`n_pilots`
    frequency bins apart (see example).

    The channel estimation header is transfered in the time domain, scaled to have `mean_power` power and inserted into the dimension `dim`
    of `sig.samples` with an offset `n_offset` from the start (i.e. in the positions `n_offset`:`n_offset`+`n_fft`*`n_repetitions`).
    Please note that this results in overwritten data payload samples. To ensure a consistent signal structure, the corresponding bit and symbol information 
    (in sig.bits[dim] and sig.symbols[dim]) are removed from the signal structure.

    

    The parameters to reproduce the channel estimation header sequence (i.e. ce_block_len, ce_block, n_pilots, pilots, n_fft, 
    n_repetitions, pilot_offset, pilot_pos, seed, mean_power) are further written into the sig.info[dim][‘ce_header’] dict.

    Examples
    --------

    The parameters n_fft=16, n_pilots=8 and pilot_offset=1 lead to the following pilot grid in the frequency domain, with
    DC: direct current component, f_N: Nyquist frequency (sample rate/2), df: frequency bin spacing (sample rate/n_fft)::

        pilot           0   P   0   P   0   P   0   P           0   P   0   P   0   P   0   P
        frequency bin   0   1   2   3   4   5   6   7           8   9   10  11  12  13  14  15
        frequency       DC  df                     f_N-df      -f_N                        -df
    
    
    Parameters
    ----------
    sig:
        signal to insert the channel estimation header into.
    dim:
        Dimension of the signal into which the frame synchrinization header should be inserted?
    offset:
        offset (in time samples) where the channel estimation header will be inserted.
    n_fft:
        Number of frequency bins (and resulting time samples) of one OFDM symbol within the channel estimation header.
    n_pilots:
        Number of frequency bins to insert random QPSK symbols.
    pilot_offset:
        Offset in frequency bins from zero frequency bin (DC component) into which the QPSK pilots will be inserted. Ths parameter
        therefore specifies the locations of the QPSK pilot symbols in the frequency domain.
    n_repetitions:
        Number of repeated OFDM symbols within the channel estimation header.
    mean_power:
        Mean power of the inserted channel estimation header.
    seed:
        Random number seed of the random number generator generating the QPSK symbols.

    Returns
    -------
        Signal with inserted channel estimation header sequence.

    See Also
    --------
    :meth:`skcomm.rx.da_channel_estimation()`    
    """
    
    if not all(isinstance(_,(int,np.integer)) for _ in (dim, offset, n_fft, n_pilots, pilot_offset, n_repetitions)):
        raise TypeError('dim, offset, n_fft, n_pilots, pilot_offset and n_repetitions must be scalar parameters of type int')
    
    if sig.n_dims < dim+1:
        raise ValueError('signal has not enough dimensions to insert frame sync header in requested dimension')
    
    if sig.sample_rate[dim]/sig.symbol_rate[dim] != 1:
        raise ValueError('signal has to sampled at 1 sample per symbol to insert frame sync header')
    
    # TODO: input arg checks (n_pilots < =n_fft/2, pilot_offset <= ?,...)
    # overall channel estimation sequence length
    ce_block_len = n_fft*n_repetitions

    if ce_block_len+offset > sig.samples[dim].size:
        raise ValueError('header to be inserted is longer than signal')

    # prepare OFDM symbol    
    ce_block = np.zeros(n_fft,dtype=np.complex128)
    # determine pilot positions according to n_pilots and pilot_offset
    pilot_pos = np.sort(np.mod(np.arange(n_fft,step=n_fft/n_pilots,dtype=int)+pilot_offset,n_fft))
    
    n_pilots = pilot_pos.size
    # generate random QPSK symbols and map them to OFDM subcarriers
    rng = np.random.default_rng(seed=seed)
    # 1/sqrt(2) due to complex signals, sqrt(mean_power) due to amplitude scaling, 1/sqrt(n_pilots) to compensate for only few pilots, n_block to compensate for 1/N scaling of IFFT (see https://numpy.org/doc/stable/reference/routines.fft.html#implementation-details)
    pilots = np.sqrt(mean_power) * 1/np.sqrt(2) * n_fft / np.sqrt(n_pilots) * ((rng.integers(low=0,high=2,size=n_pilots) - 0.5) * 2 + 1j * (rng.integers(low=0,high=2,size=n_pilots) - 0.5) * 2)
    ce_block[pilot_pos] = pilots

    # go into time domain
    ce_block = np.fft.ifft(ce_block)
    # debug
    skc_log.debug(f'mean power of sent CE block: {np.mean(np.abs(ce_block)**2):.3f}')

    # repeat CE block
    ce_block = np.tile(ce_block, n_repetitions)

    # insert header into signal (overwrite signal samples)  
    sig.samples[dim][offset:offset+ce_block_len] = ce_block
    # remove corresponding logical information (bits and symbols)
    bits_per_symb = int(np.log2(sig.constellation[dim].size))
    sig.symbols[dim] = sig.symbols[dim][ce_block_len::]
    sig.bits[dim] = sig.bits[dim][(ce_block_len*bits_per_symb)::]

    # write header info into signal
    ce_header_info = dict()
    ce_header_info['length'] = ce_block_len
    ce_header_info['header'] = ce_block
    ce_header_info['n_pilots'] = n_pilots
    ce_header_info['pilots'] = pilots
    ce_header_info['n_fft'] = n_fft
    ce_header_info['n_repetitions'] = n_repetitions
    ce_header_info['pilot_offset'] = pilot_offset
    ce_header_info['pilot_pos'] = pilot_pos
    ce_header_info['seed'] = seed
    ce_header_info['mean power'] = mean_power
    sig.info[dim]['ce_header'] = ce_header_info

    return sig

def insert_cpe_pilots(sig:signal.Signal, dim:int=0, offset:int=0, pilot_distance:int=100, mean_power:float=1.0, seed:int=12)->signal.Signal:
    """ Periodically insert pilot symbols for data-aided carrier phase estimation.

    Every `pilot_distance` sample in sig.samples[dim] is replaced by a random QPSK symbol, starting with the (`offset`-1)th sample.
    These pilot symbols can be used at the receiver to perform data-aided carrier phase estimation. To ensure a consistant 
    signal structure, the corresponding bit and symbol information (in sig.bits[dim] and sig.symbols[dim]) are removed 
    from the signal structure.

    The QPSK symbols are scaled to have `mean_power` and the random number generator is seeded with `seed`.

    The parameters to reproduce the CPE pilot sequence (i.e. `pilot_pos`, `cpe_pilots`, `pilot_overhead`) are
    further written into the sig.info[dim][cpe_pilot_info] dict.


    Parameters
    ----------
    sig:
       signal to insert the channel estimation header into.
    dim: 
        Dimension of the signal into which the pilot symbols should be inserted?
    offset:
        offset (in time samples) where the first pilot symbol will be inserted.
    pilot_distance:
        Distance (in samples) between two consecutive pilot symbols.
    mean_power:
        Mean power of the inserted pilot symbols.
    seed:
        Random number seed of the random number generator generating the QPSK symbols.

    Returns
    -------
        Signal with inserted pilot symbol sequence.

    See Also
    --------
    :meth:`skcomm.rx.pilot_aided_coarse_cpe()`  
    :meth:`skcomm.rx.cycle_slip_correction()`  
    :meth:`skcomm.rx.pilot_based_cpe_wrapper()`    
  
    """
    
    if sig.n_dims < dim+1:
        raise ValueError('signal has not enough dimensions to insert CPE pilots in requested dimension')
    
    if sig.sample_rate[dim]/sig.symbol_rate[dim] != 1:
        raise ValueError('signal has to be sampled at 1 sample per symbol to insert CPE pilots')
    
    data_len = sig.symbols[dim].size
    pilot_pos = np.arange(0, data_len, pilot_distance)
    n_pilots = pilot_pos.size
    
    # generate random QPSK symbols 
    rng = np.random.default_rng(seed=seed)
    cpe_pilots = (rng.integers(low=0,high=2,size=n_pilots) - 0.5) * 2 + 1j * (rng.integers(low=0,high=2,size=n_pilots) - 0.5) * 2
    # 1/sqrt(2) due to complex signals, sqrt(mean_power) due to amplitude scaling
    cpe_pilots = np.sqrt(mean_power) * 1/np.sqrt(2) * cpe_pilots

    # insert pilots into signal (samples)... 
    sig.samples[dim][pilot_pos+offset] = cpe_pilots
    # ... and remove corresponding logical information (symbols, bits)    
    bits_per_symb = int(np.log2(sig.constellation[dim].size))
    mask = np.ones(sig.symbols[dim].size, dtype=bool)
    mask[pilot_pos] = False
    sig.symbols[dim] = sig.symbols[dim][mask]
    sig.bits[dim] = sig.bits[dim].reshape(-1, bits_per_symb)[mask].reshape(-1)

    pilot_overhead = pilot_pos.size/sig.symbols[dim].size
    # debug
    skc_log.debug(f'CPE pilot overhead (Dim {dim:d}): {pilot_overhead*100:.2f}%') 
    
    # write CPE pilot info into signal
    cpe_pilot_info = dict()
    cpe_pilot_info['pilot_pos'] = pilot_pos
    cpe_pilot_info['cpe_pilots'] = cpe_pilots
    cpe_pilot_info['cpe_pilot_overhead'] = pilot_overhead      
    sig.info[dim]['cpe_pilot_info'] = cpe_pilot_info

    return sig