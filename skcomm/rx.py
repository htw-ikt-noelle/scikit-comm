""" 
.. autosummary::

    blind_adaptive_equalizer
    calc_eq_frequency_response_zf
    carrier_phase_estimation_VV
    carrier_phase_estimation_bps
    combining
    count_errors
    coRx_phase_imbalance_comp
    cycle_slip_correction
    da_cfo_estimation
    da_channel_estimation
    da_equalizer
    da_frame_sync    
    da_snr_estimation
    dc_block_and_unit_variance_norm
    decision
    demapper
    deskew
    frequency_offset_correction
    pilot_aided_coarse_cpe
    pilot_based_cpe_wrapper
    sampling_clock_adjustment
    sampling_phase_adjustment
    stokesEQ_pdm
    symbol_sequence_sync

"""

import warnings
warnings.simplefilter('always', UserWarning) # show all user warnings, even repetations in loops
import logging
import numpy as np
import scipy.signal as ssignal
import scipy.interpolate as sinterpolate
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter1d

from . import utils
from . import filters
from . import signal
from . import channel
from . import visualizer

try:
    from .cython_mods import rx_cython
    cython_available = True
except ImportError:
    cython_available = False

skc_log = logging.getLogger(__name__)

def demapper(samples, constellation):
    """    
    Demap samples to bits using a given constellation alphabet.

    samples are compared to a given constellation alphabet and the index
    of the corresponding constellation (integer) is
    converted to the corresponding bit value.   
    
    The resulting bit sequence is therefore of length: 
    np.log2(constellation.size)*samples.size.

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        sampled input signal.
    constellation : 1D numpy array, real or complex
        possible constellation points of the input signal.    

    Returns
    -------
    bits : 1D numpy array, bool
        converted bit sequence.

    """
    samples = np.asarray(samples)
    constellation = np.asarray(constellation)

    if constellation.ndim > 1:
        raise ValueError('number of dimensions of constellation must not exceed 1!')

    if samples.ndim > 1:
        raise ValueError('number of dimensions of samples must not exceed 1!')

    decimals = np.full_like(samples.real, np.nan)
    bps = int(np.log2(constellation.size))    
    
    for const_idx, const_point in enumerate(constellation):
        decimals[samples == const_point] = const_idx
    
    # convert constellation index (decimal) to bits
    bits = utils.dec_to_bits(decimals, bps)        
  
    return bits

def decision(samples, constellation, norm=True):
    """
    Decide samples to a given constellation alphabet.

    Find for every sample the closest constellation point in a
    constellations array and return this value.    

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        sampled input signal.
    constellation : 1D numpy array, real or complex
        possible constellation points of the input signal.
    norm : bool
        should the samples be normalized (to mean maginitude of constellation)
        before decision?        

    Returns
    -------
    dec_symbols : 1D numpy array, real or complex
        clostest constellation point for every input sample.

    """    
    if constellation.ndim > 1:
        raise ValueError('number of dimensions of constellation must not exceed 1!')

    # if samples.ndim > 2:
    #     raise ValueError('number of dimensions of samples should be <= 2')
    if samples.ndim > 1:
        raise ValueError('number of dimensions of samples must not exceed 1!')        
    
    if norm:
        # normalize samples to mean magnitude of original constellation
        mag_const = np.mean(abs(constellation))
        # mag_samples = np.mean(abs(samples), axis=-1).reshape(-1,1)
        mag_samples = np.mean(abs(samples))
        samples_norm = samples * mag_const / mag_samples
    else:
        samples_norm = samples

    idx = np.argmin(np.abs(samples_norm - constellation.reshape(-1,1)), axis=0)
    dec_symbols = constellation[idx]
    return dec_symbols

def count_errors(bits_tx, bits_rx):
    """
    Count bit errors.
    
    Count the bit error rate (BER) by comparing two bit sequences. Additionally
    also the position of the bit errors is returned as a bool array of size
    bits_tx.size, where True indicates a bit error.
    
    If the bit sequence bits_rx is longer than the sent sequency, the sent sequence
    is repeated in order to match both lengths.
     

    Parameters
    ----------
    bits_tx : 1D numpy array, bool
        first bits sequence.
    bits_rx : 1D numpy array, bool
        first bits sequence.


    Returns
    -------
    results : dict containing following keys
        ber : float
            bit error rate (BER)
        err_idx : 1D numpy array, bool
            array indicating the bit error positions as True.

    """
    if (bits_rx.ndim > 1) | (bits_tx.ndim > 1):
        raise ValueError('number of dimensions of bits must not exceed 1!')
        
    if bits_tx.size > bits_rx.size:
        raise ValueError('number of bits transmitted must not exceed number of received bits!')
    
    # if bit sequences are of unequal length, repeat bits_tx accordingly
    if bits_tx.size < bits_rx.size:
        ratio_base = bits_rx.size // bits_tx.size
        ratio_rem = bits_rx.size % bits_tx.size        
        bits_tx = np.concatenate((np.tile(bits_tx, ratio_base), bits_tx[:ratio_rem]), axis=0)
    
    # count errors
    err_idx = np.not_equal(bits_tx, bits_rx)
    ber = np.sum(err_idx) / bits_tx.size
    
    # generate output dict
    results = dict()
    results['ber'] = ber
    results['err_idx'] = err_idx
    return results

def sampling_phase_adjustment(samples:np.ndarray, sample_rate:float=1.0, symbol_rate:float=2.0, shift_dir:str='both',delay:float=0.0, compensate:bool=True)->dict:
    """
    Estimate the sampling phase offset and compensate for it.
    
    The sampling phase offset is estimated by finding the phase of the oszillation
    with the frequency of the symbol rate in the signal abs(samples)**2. This offset
    is compensated for by a temporal cyclic shift of the input signal.
    
    To contain this frequency component, the signal has to be sampled at least 
    with a rate of three times the symbol rate. If the input signal is sampled
    with lower frequency, the signal is temporally upsampled.    

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        input signal.        
    sample_rate : float, optional
        sample rate of input signal in Hz. The default is 1.0.
    symbol_rate : float, optional
        symbol rate of input signal in Hz. The default is 2.0.
    shift_dir : string, optional
        defines the bahaviour of the compensation of the found temporal shift. Can be 
        either 'delay', 'advance' or 'both'. 
        'delay' / 'advance' will only compensate for the time shift by exclusively 
        shifting the signal to the right / left. This means that the time shift 
        can be in the range between [-1/symbol_rate, 0] or [0, 1/symbol_rate], respectively. 
        The option 'both' will shift the signal either to the left or to the 
        right, depending on the minimum amount of time shift. The time shift will 
        therefore be in the range between [-0.5/symbol_rate, 0.5/symbol_rate].
    delay : float, optional
        additional delay in seconds to apply on the signal after timing compensation. The dafault value is 0.0
    compensate : bool, optional
        should the sampling skew compensation and additional delay be applied? The default is True.

    Returns
    -------
    results : dict containing following keys
        samples_out : 1D numpy array, real or complex
            cyclic shifted output signal.
        est_shift : float
            estimated (and inversely applied) temporal shift.

    """    
    # do all dsp on a copy of the samples
    samples_tmp = samples
    # sample rate of dsp (must be at least 3 time symbol rate)
    sr_dsp = sample_rate
    
    # if signal has less than three samples per symbol --> oszillation with
    # symbol rate not present in spectrum of abs(signal)
    if sample_rate < (3 * symbol_rate):
        # upsample to 3 samples per symol
        sr_dsp = symbol_rate * 3
        # watch out, that this is really an integer
        len_dsp = sr_dsp / sample_rate * np.size(samples_tmp, axis=0)
        if len_dsp % 1:
            #hotfix 
            for i in range(np.size(samples_tmp, axis=0)):
                samples_tmp = samples_tmp[:-1]
                len_dsp = sr_dsp / sample_rate * np.size(samples_tmp, axis=0)
                if len_dsp % 1:
                    #raise ValueError('DSP samplerate results in asynchronous sampling of the data symbols')
                    pass
                else:
                    break

        samples_tmp = ssignal.resample(samples_tmp, num=int(len_dsp), window=None)
    
    # calc length of vector so that spectrum exactly includes the symbol rate
    tmp = np.floor(symbol_rate * np.size(samples_tmp, axis=0) / sr_dsp)
    n_sam = int(tmp / symbol_rate * sr_dsp)
    
    
    # cut vector to size and take amplitude square
    samples_tmp = np.abs(samples_tmp[:n_sam])**2
    
    # calc phase of frequency component with frequency equal to the symbol rate
    t_tmp = np.arange(n_sam) / sr_dsp
    est_phase = np.angle(np.sum(samples_tmp * np.exp(1j * 2 * np.pi * symbol_rate * t_tmp)))
    
    # ensure to only advance the signal (shift to the left)
    if (shift_dir == 'advance') and (est_phase < 0.0):
        est_phase += 2 * np.pi        
    
    # ensture to only delay the singal (shift to the right)
    if (shift_dir == 'delay') and (est_phase > 0.0):
        est_phase -= 2 * np.pi        
    
    est_shift = est_phase / 2 / np.pi / symbol_rate
    
    # # for debugging purpose
    # sps = int(sample_rate / symbol_rate)
    # visualizer.plot_eye(samples, sample_rate=sample_rate, bit_rate=symbol_rate)
    # plt.plot(np.abs(samples[:10*sps]), 'C0')
    
    if compensate:
        # compensate for found sample phase offset
        samples = filters.time_shift(samples, sample_rate, -est_shift + delay)
    
    # # for debugging purpose
    # visualizer.plot_eye(samples, sample_rate=sample_rate, bit_rate=symbol_rate)
    # plt.plot(np.abs(samples[:10*sps]), 'C1')
    
    # generate results dict
    results = dict()
    results['samples_out'] = samples
    results['est_shift'] = est_shift
    
    return results

def sampling_clock_adjustment(samples, sample_rate=1.0, symbol_rate=2.0, block_size=500):
    """
    Estimate sampling frequency offset and correct for it.
    
    This function estimates the sampling frequency offset between nominal (given) 
    and actual (sample rate of samples) sampling frequency and corrects for the
    found value.
    This is done by splitting the singal into blocks of block_size SYMBOLS and 
    estimating the sampling time offset for each of the blocks (see skcomm.rx.sampling_phase_adjustment).
    Then, the sampling time offest is corrected for by cyclic time shift of the individual blocks.
    Longer block sizes enhance the accuracy of the time offset estimatin, but cause the sampling
    clock offset to be larger within one block.
    CAUTION: method will fail for large sampling clock (frequency) offsets and if the sampling frequency
    offset leads to a timing error larger than +- 1/symbol_rate over the whole samples.
    

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        input signal.        
    sample_rate : float, optional
        sample rate of input signal in Hz. The default is 1.0.
    symbol_rate : float, optional
        symbol rate of input signal in Hz. The default is 2.0.
    block_size : int, optional
        signal is split into blocks of block_size SYMBOLS. The default is 500.

    Returns
    -------
    results : dict containing following keys
        samples_out : 1D numpy array, real or complex
            output signal.
        est_shift : float or 1D numpy array of floats
            estimated (and inversely applied) temporal shift per block.

    """
    
    # if only one block is questioned -> do only time shift
    if block_size == -1:
        results = sampling_phase_adjustment(samples, sample_rate=sample_rate,
                                                         symbol_rate=symbol_rate, 
                                                         shift_dir='both')
        return results
    # otherwise, do time shift for every block
    else:
        n_samples = np.size(samples, axis=0)    
        sps = int(sample_rate / symbol_rate)
        s_per_block = block_size * sps    
        
        # cut signal, to an integer number of blocks and create array
        n_samples_new = int(np.floor(n_samples / s_per_block) * s_per_block)
        samples_array = np.reshape(samples[:n_samples_new], (-1, s_per_block))
        
        tmp = list()
        # run sampling_phase_adjustment once per block
        for idx, block in enumerate(samples_array):
            tmp.append(sampling_phase_adjustment(block, sample_rate=sample_rate,
                                                             symbol_rate=symbol_rate, 
                                                             shift_dir='both'))
            
        # Warning in case samples have to be dropped due to non-ideal block size
        if n_samples_new < n_samples:
            warnings.warn('Due to n_samples not being a multiple of the samples_per_block, {}  samples had to be dropped!'.format((n_samples - n_samples_new)))

            
    # generate output dict containing samples and estimated time shifts per block
    results = dict()
    results['samples_out'] = np.asarray([block['samples_out'] for block in tmp]).reshape(-1)
    results['est_shift'] = np.asarray([block['est_shift'] for block in tmp])
    return results



    ########### MULTIPLE, DIFFERENT METHODS TO COMPENSATE SAMPLING CLOCK OFFSETS (have to be tested) ######################
    """
    These could be used instead of the above implemented method (within the "else" branch)
    
    METHOD 1: "sample dropping", prabably better for real time implementation, but not better in first tests
    
        n_blocks = np.size(samples_array, axis=0)        
        correction  = 0
        tmp = list()
        # run sampling_phase_adjustment multiple times
        for block in np.arange(n_blocks):
            # shift estimation, "dry run"
            samples_tmp = samples[block*s_per_block + correction:block*s_per_block + s_per_block + correction]
            tmp = sampling_phase_adjustment(samples_tmp, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='advance')
            # "sample dropping"
            correction += int(tmp['est_shift']//(1/sample_rate))
            # actual phase adjustemnt
            samples_block = samples[block*s_per_block + correction:block*s_per_block + s_per_block + correction]
            tmp.append(sampling_phase_adjustment(samples_block, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='advance'))
            
            
    METHOD 2: estimate slope of sampling clock offset over blocks and do resampling (only works in case of almost constant sampling frequency missmatch)
        
        tmp = list()
        # run sampling_phase_adjustment multiple times, once for every block
        for idx, block in enumerate(samples_array):
            tmp.append(sampling_phase_adjustment(block, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='both'))
            # skc_log.info(results[-1]['est_shift'])
        # generate shifts as ndarrays    
        shifts = np.asarray([block['est_shift'] for block in tmp])
        # do unwrap of shifts (has to be converted in rad???), and shifts have to be fftshifted, because of current filter implementation...can be changed in the future???
        shifts = np.unwrap(np.fft.fftshift(shifts) / (0.5/symbol_rate) * 2 * np.pi) * (0.5/symbol_rate) / 2 / np.pi
        # estimate mean sample time offset per block
        sample_time_offset = np.mean(np.diff(shifts))
        
        t_block = s_per_block / sample_rate
        # ratio between nominal and actual sampling frequency
        ratio = t_block / (t_block + sample_time_offset)
        
        t_old = np.arange(n_samples) / sample_rate
        # interpolate signal at different timing / sampling instants, but keep start and end time the same
        n_samples_new = int(np.round(ratio * n_samples))
        t_new = np.linspace(start=t_old[0], stop=t_old[-1], num=n_samples_new, endpoint=True)        
        f = sinterp.interp1d(t_old, samples, kind='cubic')
        samples = f(t_new)
        
        # do clock phase adjumstemnt once after resampling        
        results = sampling_phase_adjustment(samples, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='both')
    """
    #####################################################################################

def deskew(samples_1:np.ndarray[np.complex128 | np.float64], samples_2:np.ndarray[np.complex128 | np.float64], sample_rate:float=1.0, symbol_rate:float=2.0, cm_delay:float=0.0, compensate:bool=True)->dict:
    """
    Finds the skew between two signals (and compensates for it).

    The delay (sampling phase offset) to the ideal sampling instant within the range of [0, 1/symbol_rate] is found
    for each of the signals. For more information see :meth:`skcomm.rx.sampling_phase_adjustment()`.

    The found delay can be compensated for for each signal individually, and therefore performing a deskewing.

    Parameters
    ---------- 
    samples_1: 
        input signal 1.
    samples_2: 
        input signal 2.        
    sample_rate: 
        sample rate of input signals in Hz. 
    symbol_rate:
        symbol rate of input signals in Hz.
    cm_delay: 
        common delay in seconds to apply on both signals after skew compensation.
    compensate: 
        should skew compensation and cm_delay be applied?
    
    Returns
    ------- 
    
    results with following keys
        * 'samples_out_1' : np.ndarray[np.complex128 | np.float64] 
            output signal 1, if parameter compensate==False, the output signal 1 is equal to the input singal 1.
        * 'samples_out_2'  : np.ndarray[np.complex128 | np.float64] 
            output signal 2, if parameter compensate==False, the output signal 2 is equal to the input singal 2.
        * 'est_shift_1' : float 
            estimated (and inversely applied) temporal shift in seconds for signal 1.
        * 'est_shift_2' : float 
            estimated (and inversely applied) temporal shift in seconds for signal 2.
    
    See Also
    --------
    :meth:`skcomm.rx.sampling_phase_adjustment()`
    :meth:`skcomm.filters.time_shift()`


    """

    result_1 = sampling_phase_adjustment(samples_1, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='both', compensate=False)
    
    samples_2 = filters.time_shift(samples_2, sample_rate=sample_rate, tau = -result_1['est_shift'])
    result_2 = sampling_phase_adjustment(samples_2, sample_rate=sample_rate, symbol_rate=symbol_rate, shift_dir='both', compensate=False)
    
    if compensate:
        samples_1 = filters.time_shift(samples_1,sample_rate=sample_rate,tau=-result_1['est_shift'] + cm_delay) 
        samples_2 = filters.time_shift(samples_2,sample_rate=sample_rate,tau=-result_2['est_shift'] + cm_delay)

    result_2['est_shift'] = result_2['est_shift'] + result_1['est_shift']
    skc_log.info(f"estimated shift 1: {result_1['est_shift']*symbol_rate:.2f} UI, esimated shift 2: {result_2['est_shift']*symbol_rate:.2f} UI, estimated skew (1-2): {(result_1['est_shift']-result_2['est_shift'])*symbol_rate:.2f} UI")

    results = dict()
    results['samples_out_1'] = samples_1
    results['samples_out_2'] = samples_2
    results['est_shift_1'] = result_1['est_shift']
    results['est_shift_2'] = result_2['est_shift']
    return results

def carrier_phase_estimation_VV(symbols, n_taps=31, filter_shape='wiener', mth_power=4, mag_exp=0, rho=0.1):
    """
    Viterbi-Viterbi carrier phase estimation and recovery.
    
    This function estimates the phase noise of the carrier using the Viterbi-Viterbi
    method [1]. Either a rectangular or a Wiener filter shape can be applied for
    phase averaging.    
    

    Parameters
    ----------
    symbols :  1D numpy array, real or complex
        input symbols.  
    n_taps : int, optional
        Number of taps of the averaging filter. Must be an odd number.
        The default is n_taps = 31.
    filter_shape : string, optional
        Specifies the averaging filter shape (window function): either 'rect', 
        'wiener', 'hyperbolic' or 'lorentz'.
        The default is filter_shape = 'wiener'.
    mth_power:  int, optional
        Specifies the power to which the symbols are raised to remove the data 
        modulation (i.e., the number of equidistant modulation phase states).
        The default is mth_power = 4 (corresponding to QPSK).
    mag_exp : optional
        Specifies the exponent, to which the symbol magnitudes are raised before 
        averaging the phasors. A value > 0 leads to the preference of the outer 
        symbols in the averaging process, while a value <0 accordingly leads to 
        a preference of inner symbols. 
        For mag_exp = 0, the symbol magnitudes are ignored in the phase estimation
        process (For more information see [1]).
        The default value is mag_exp = 0.
    rho : float, optional, rho>0
        Shape parameter for 'wiener', 'hyperbolic' and 'lorentz' filter. 
        For larger rho, the filter shape becomes narrower.
        For 'wiener' filter shape,  rho is the ratio between the magnitude of
        the frequency noise variance σ²_ϕ and the (normalized) AWGN variance σ²_n' 
        (for more information see [2],[3]). σ²_ϕ is related to the laser 
        linewidth LW as σ²_ϕ = 2*π*LW/symbol_rate.
        For 'hyperbolic' and 'lorentz' filter shape (aka Cauchy or Abel window), 
        1/rho is the FWHM parameter of the filter shape.
        The default is rho = 0.1.

    Returns
    -------
     results : dict containing following keys
        rec_symbols : 1D numpy array, real or complex
            recovered symbols.
        phi_est : 1D numpy array, real
            estimated phase noise random walk
        cpe_window: 1D numpy array, real
            applied CPE slicing-average window ()
    
    References
    ----------
    [1] A. Viterbi, "Nonlinear estimation of PSK-modulated carrier phase with 
    application to burst digital transmission," in IEEE Transactions on 
    Information Theory, vol. 29, no. 4, pp. 543-551, July 1983, doi: 10.1109/TIT.1983.1056713.
    
    [2] Ezra Ip, Alan Pak Tao Lau, Daniel J. F. Barros, and Joseph M. Kahn, 
    "Coherent detection in optical fiber systems," Opt. Express 16, 753-791 (2008)
    
    [3] E. Ip and J. M. Kahn, "Feedforward Carrier Recovery for Coherent 
    Optical Communications," in Journal of Lightwave Technology, vol. 25, 
    no. 9, pp. 2675-2692, Sept. 2007, doi: 10.1109/JLT.2007.902118.
    
    [4]  Wolfram Language & System Documentation Center, 
    https://reference.wolfram.com/language/ref/CauchyDistribution.html,
    https://en.wikipedia.org/wiki/Cauchy_distribution
    """    
    # TODO: change input argument 'symbols' to class Signal()
    
    # input sanitization (using only real value of first element)
    n_taps = np.real(np.atleast_1d(n_taps)[0])
    filter_shape = np.atleast_1d(filter_shape )[0]
    mth_power = np.real(np.atleast_1d(mth_power)[0])
    mag_exp = np.real(np.atleast_1d(mag_exp)[0])
    rho = np.real(np.atleast_1d(rho)[0])
        
    if n_taps < 1  or n_taps%1 != 0  or  n_taps%2==0:
        raise ValueError('The number of CPE taps n_taps must be an odd integer ≥1.')
    
    if symbols.size <= 4*n_taps:
        raise ValueError('The number of symbols must exceed 4×n_taps in CPE averaging filter!')
        
    if mth_power < 1 or mth_power%1:
        raise ValueError('The parameter mth_power must be an integer ≥1')
        
    if not rho > 0:
        raise ValueError('The parameter rho must be >0')
    
    # for all filter_shapes: remove modulation of symbols (suitable only for MPSK formats with equidistant symbol-phase allocation)
    # raise unit-magnitude symbols to m-th power
    raised_symbols = np.exp(1j*np.angle(symbols)*mth_power)
    if mag_exp != 0: # exponentiate also the symbol magnitude (i.e. weighting of symbol phasors prior to filtering)
        raised_symbols = (np.abs(symbols)**mag_exp) * raised_symbols

    # smooth (slicing average) exponentiated symbols with non-causal FIR filter and estimate phase-noise random-walk
    wn = np.zeros(symbols.shape) # template impulse response for phase avg. filter
    
    # postive-time part of truncated window function (filter impulse response)
    if filter_shape == 'rect': # rectangular slicing filter
        wn[0: (n_taps//2 + 1)] = 1
    elif filter_shape == 'wiener':
        a = 1 + rho / 2 - np.sqrt( ( 1 + rho / 2)**2 - 1) # helper variable alpha
        wn[0: (n_taps//2 + 1)] = a * rho / (1 - a**2) * a**np.arange(n_taps // 2 + 1)
    elif filter_shape == 'hyperbolic':
        #wn[0: (n_taps//2 + 1)] = 1/(1+np.arange(n_taps//2+1))
        b = 2*rho
        wn[0: (n_taps//2 + 1)] = 1/(1 + b*np.arange(n_taps//2+1))
    elif filter_shape == 'lorentz':
        b = 0.5/rho
        wn[0: (n_taps//2 + 1)] = 1/(b*np.pi*(1 + np.arange(n_taps//2+1)**2/b**2))
    else:
        raise TypeError("filter_shape '" + filter_shape + "' not implemented yet")

    # negative-time part (symmetrical to positive-time part)
    wn[-n_taps//2+1::] = np.flip(wn[1:(n_taps//2 + 1)])
    wn = wn / np.sum(wn) # optional: normalize to unit-sum (make unbiased estimator)

    # apply averaging filter in frequency domain (no group delay)
    symb_filtered = filters.filter_samples(raised_symbols, np.fft.fftshift(np.fft.fft(wn)) , domain='freq')
    # extract phase, unwrap and rescale
    phi_est = 1/mth_power * np.unwrap(np.angle(symb_filtered))
    
    # for QPSK: rotate recovered constellation by pi/4 (as usual in context of digital communication)
    if mth_power == 4:
        phase_correction = np.pi/4
    else:
        phase_correction = 0
    
    # phase recovery: subract estimated phase-noise from input symbols
    rec_symbols = symbols * np.exp(-1j*(phi_est + phase_correction))
    # crop start and end (due to FIR induced delay)
    rec_symbols = rec_symbols[1*n_taps+1:-n_taps*1]
    phi_est = phi_est[1*n_taps+1:-n_taps*1]
    
    # for plotting / return
    cpe_window = np.concatenate((wn[-n_taps//2+1::],wn[0: (n_taps//2 + 1)]), axis=0)
    # for debugging
    if False:
        plt.figure()
        plt.stem(np.arange(-cpe_window.size//2+1,cpe_window.size//2+1),cpe_window)
        plt.title('applied CPE window function'); plt.xlabel('tap number');
        plt.ylabel('tap value'); ax = plt.gca(); ax.grid(axis='y'); plt.show()
    
    # generate output dict containing recovered symbols, estimated phase noise and CPE filter taps
    results = dict()
    results['rec_symbols'] = rec_symbols[1*n_taps+1:-n_taps*1]
    results['phi_est'] = phi_est # units [rad]
    results['cpe_window'] = cpe_window # center tap = zero-delay (t=0)
    return results

def _cpe_bps_loop(samples_norm, samples_out, dec_samples, est_phase_noise, errors, n_blocks, n_taps, rotations, constellation):
    """
    Helper function which implements the actual loop for the blind phase search
    carrier phase estimation (CPE).
    
    For explanation and help regarding the input and output parameters see
    docu of comm.rx.carrier_phase_estimation_bps().
    """
    
    for block in range(n_blocks):
        for idx, rotation in enumerate(rotations):
            # rotate block by test phases
            rotated_samples = samples_norm[block*n_taps:(block+1)*n_taps] * rotation
            # decide nearest constellation points for each sample in block for particular test phase
            dec_samples[:,idx] =  constellation[np.argmin(np.abs(rotated_samples - constellation.reshape(-1,1)), axis=0)]    
            # calc error for particular test phase
            errors[idx] = np.sum(np.abs(rotated_samples - dec_samples[:,idx])**2)                
        samples_out[block, :] = dec_samples[:,np.argmin(errors)]        
        est_phase_noise[block] = np.angle(rotations[np.argmin(errors)])
    
    return samples_out, est_phase_noise

def  carrier_phase_estimation_bps(samples, constellation, n_taps=15, n_test_phases=15, const_symmetry=np.pi/2, interpolate=True, exec_mode='auto'):
    """
    "Blind phase search" carrier phase estimation and recovery.
    
    This method implements a slight modification of the carrier phase estimation
    and recovery method proposed in [1].
    
    A block of n_taps samples of the input signal (at 1 sample per symbol) is 
    rotated by n_test_phases individual, equally spaced phases between 
    -const_symmetry/2 and const_symmetry.
    
    The rotation phase producing a smallest squared error between these rotated 
    samples and the decided constellation points of the original constellation
    is assumed to be the phase error produced by the channel (or laser(s)).
    
    Additional to the decided (ideal) constellation per block (as proposed in [1]),
    also the unwraped estimated random phase walk is calculated and interpolated.
    This estimated phase walk is subtracted from the phases of the input samples
    and therefore used to recover the carrier phase of the signal.        

    Parameters
    ----------
    samples : 1D numpy array, real or complex
        input symbols.
    constellation : 1D numpy array, real or complex
        constellation points of the sent (original) signal.
    n_taps : int, optional
        numer of samples processed in one block. The default is 15.
    n_test_phases : int, optional
        number of phases which are tested. Defines the accuracy of the phase
        estimation method. The default is 15.
    const_symmetry : float, optional
        symmetry (ambiguity) of the original constellation points. 
        The default is pi/2.
    exec_mode: string
        Controls if Python or os-specific Cython implementation is used. 
        'auto': use Cython module if available, otherwise use Python implementation.
        'python': force to use Python implementation, even if a Cython module is available.
        'cython': force to use the Cython module, might cause an error if Cython module is not available.
        The default is 'auto'.

    Returns
    -------
     results : dict containing following keys
        samples_out : 1D numpy array, real or complex
            decided constellation points of the signal closest to the estimated
            random phase walk.
        samples_corrected : 1D numpy array, real or complex
            input symbols with recovered carrier phase.
        est_phase_noise : 1D numpy array, real
            estimated (unwraped) random phase walk.
            
    References
    ----------
    [1] T. Pfau, S. Hoffmann and R. Noe, "Hardware-Efficient Coherent Digital 
    Receiver Concept With Feedforward Carrier Recovery for M -QAM Constellations," 
    in Journal of Lightwave Technology, vol. 27, no. 8, pp. 989-999, April15, 2009, 
    doi: 10.1109/JLT.2008.2010511.
    """

    if exec_mode=='auto':
        if cython_available:
            exec_mode = 'cython'
        else:
            exec_mode = 'python'

    # normalize samples to constellation    
    mag_const = np.mean(abs(constellation))
    mag_samples = np.mean(abs(samples))
    samples_norm = samples * mag_const / mag_samples
    
    n_blocks = samples_norm.size // n_taps
    
    # generate all requested rotation phases
    rotations = np.exp(1j*np.arange(-const_symmetry/2, const_symmetry/2, const_symmetry/n_test_phases))
    
    errors = np.full(n_test_phases,fill_value=np.nan, dtype=np.float64)
    dec_samples = np.full((n_taps,n_test_phases), fill_value=np.nan, dtype=np.complex128)    
    samples_out = np.zeros([n_blocks, n_taps], dtype=np.complex128)
    est_phase_noise = np.zeros(n_blocks)
    
    if exec_mode=='cython':
        samples_out, est_phase_noise = rx_cython._cpe_bps_loop(samples_norm, samples_out, dec_samples, est_phase_noise, errors, n_blocks, n_taps, rotations, constellation)
    elif exec_mode=='python':
        samples_out, est_phase_noise = _cpe_bps_loop(samples_norm, samples_out, dec_samples, est_phase_noise, errors, n_blocks, n_taps, rotations, constellation)
    
    samples_out = np.asarray(samples_out).reshape(-1)
    
    # interpolate phase nosie between blocks onto symbols        
    unwrap_limit = 2 * np.pi / const_symmetry
    est_phase_noise = np.unwrap(np.asarray(est_phase_noise)*unwrap_limit)/unwrap_limit

    # linearly interpolate phases between blocks, or ...
    if interpolate:
        f_int = sinterpolate.interp1d(np.arange(n_blocks)*n_taps, est_phase_noise, kind='linear', bounds_error=False, fill_value='extrapolate')
        est_phase_noise_int = f_int(np.arange(n_blocks*n_taps))        
     # correct with constant phase per block or ...
    else:
        est_phase_noise_int = np.tile(est_phase_noise,[n_taps,1]).T.flatten()        
        # f_int = sinterpolate.interp1d(np.arange(n_blocks)*n_taps, est_phase_noise, kind='previous', bounds_error=False, fill_value='extrapolate')
        # est_phase_noise_int = f_int(np.arange(n_blocks*n_taps))
    
    

    if n_blocks == 1:
        est_phase_noise_int = np.full(est_phase_noise_int.shape, fill_value=est_phase_noise)
    
    samples_corrected = samples_norm[:n_blocks*n_taps] * np.exp(1j * est_phase_noise_int)
    
    # generate output dict containing recoverd symbols and estimated phase noise
    results = dict()
    results['samples_out'] = samples_out
    results['samples_corrected'] = samples_corrected
    results['est_phase_noise'] = np.asarray(est_phase_noise_int)
    return results

def symbol_sequence_sync(sig, dimension=-1):
    """
    Estimate and compensate delay and phase shift between reference symbol / bit sequence and physical samples.
    
    The sig.samples have to be sampled at a rate of one sample per symbol. 
    
    A complex correlation between sig.samples and sig.symbols is performed in 
    order to find the delay and the phase shift between both signals. 
    
    Further also a complex correlation between conj(sig.samples) and sig.symbols 
    is performed in order to detect a flip (inversion) of the imaginary part.
    
    The found dalay is compensated for by cyclic shift (roll) of the reference symbol
    and bit sequence (sig.symobls and sig.bits, respectively) while the ambiguity 
    (phase shift and flip of imaginary part) is compensated for by manipulating
    the physical samples (sig.samples).
    
    This operation can be performed to one specific dimension of the signal or 
    to all dimensions of the signal independently.
    

    Parameters
    ----------
    sig : skcomm.signal.Signal
        signal containing the sequences to be synced.

    dimension : int
        dimension of the signal to operate on. If -1 the synchronization is performed
        to all dimensions of the signal. The default is -1.

    Returns
    -------
    return_dict : dict containing following keys
        number of dimension of signal : dict containing following keys
            phase_est : float
                estimated phase offset of specified signal dimension
            symbol_delay_est : int
                estimated symbol offset of specified signal dimension
        sig :  sskcomm.signal.Signal
            signal containing the synced sequences (all signal dimensions)

    """    
    if not isinstance(sig, signal.Signal):
        raise TypeError("input parameter must be of type 'skcomm.signal.Signal'")
        
    if dimension == -1:
        dims = range(sig.n_dims)
    elif (dimension >= sig.n_dims) or (dimension < -1):
        raise ValueError("-1 <= dimension < sig.n_dims")
    else:
        dims = [dimension]
    
    # init dict for return values
    return_dict = {}

    # iterate over specified signal dimensions
    for dim in dims:
    
        # use only one symbol sequence for correlation
        corr_len = sig.symbols[dim].size
        
        # complex correlation of received, sampled signal and referece symbole sequence
        corr_norm = utils.find_lag(sig.samples[dim][:corr_len], sig.symbols[dim], period_length=None)
        # complex correlation of received, sampled and conjugated signal and referece symbole sequence
        corr_conj = utils.find_lag(np.conj(sig.samples[dim][:corr_len]), sig.symbols[dim], period_length=None)
        
        # decide which of the correlations is larger and determine delay index and phase from it
        if np.abs(corr_norm['max_correlation']) > np.abs(corr_conj['max_correlation']):
            symbols_conj = False
            symbol_delay_est = corr_norm['estimated_lag']
            phase_est = np.angle(corr_norm['max_correlation'])
        else:
            symbols_conj = True
            symbol_delay_est = corr_conj['estimated_lag']
            phase_est = np.angle(corr_conj['max_correlation'])   
        
        # quantize phase to mulitples of pi/2, because large noise can alter the phase of the correlation peak...
        # quantization therefore ensures phase rotations to be only multiples of pi/2 
        # TODO:CHECK if this is reasonable for every modulation format, if not, add a case decision here depending on modulation format!!!
        phase_est = np.round(phase_est / (np.pi/2)) * (np.pi/2)
        
        # for debugging purpose
        # skc_log.info('conjugated:{}, delay in symbols={}, phase={}'.format(symbols_conj, symbol_delay_est, phase_est))
        # plt.plot(np.abs(corr_norm))
        # plt.plot(np.abs(corr_conj))
        # plt.show()
        
        # manipulate logical reference symbol sequence and bit sequences in order to compensate 
        # for delay 
        bps = np.log2(sig.constellation[dim].size)
        sig.symbols[dim] = np.roll(sig.symbols[dim], int(symbol_delay_est)) 
        sig.bits[dim] = np.roll(sig.bits[dim], int(symbol_delay_est*bps)) 
        
        # return symbol delay est & init nested dict
        return_dict[dim] = {"symbol_delay_est": int(symbol_delay_est)}  

        # manipulate physical samples in order to compensate for phase rotations and inversion 
        # of real and / or imaginary part (optical modulator ambiguity)
        if symbols_conj:    
            sig.samples[dim] = np.conj(sig.samples[dim] * np.exp(1j*phase_est))
            return_dict[dim]["phase_est"] = phase_est        
        else:        
            sig.samples[dim] = sig.samples[dim] * np.exp(-1j*phase_est)
            return_dict[dim]["phase_est"] = -phase_est   

    # return signal object
    return_dict["sig"] = sig
    
    return return_dict 

def _bae_loop(samples_in, samples_out, h, n_taps, sps, n_CMA, mu_cma, n_RDE, 
              mu_rde, radii, mu_dde, stop_adapting, sig_constellation, r, 
              shift, return_info, h_tmp, eps_tmp):
    """
    Helper function which implements the actual loop for the blind 
    adaptive equalizer (bae).
    
    For explanation and help regarding the input and output parameters see
    docu of comm.rx.blind_adaptive_equalizer().
    """
    
    n_update = 0
    
    # equalizer loop
    for sample in range(0, samples_out.size, shift):        
        # filter the signal for each desired output sample (convolution)
        # see [1], eq. (5)
        samples_out[sample] = np.sum(h * samples_in[n_taps+sample:sample:-1])        
        
        # for each symbol, calculate error signal... 
        if (sample % sps == 0):            
            # in CMA operation case
            if sample <= n_CMA:
                # calc error, see [1], eq. (26)
                eps = samples_out[sample] * (np.abs(samples_out[sample])**2 - r) 
                mu = mu_cma
            # in DDE operation case
            elif sample > (n_CMA + n_RDE):
                # decision (find closest point of original constellation)                    
                idx = np.argmin(np.abs(samples_out[sample] - sig_constellation))
                const_point = sig_constellation[idx]
                eps = (samples_out[sample] - const_point)
                mu = mu_dde
            # in RDE operation case
            else:
                # decision (find closest radius of original constellation)                    
                r_tmp = radii[np.argmin(np.abs(np.abs(samples_out[sample])**2 - radii))]
                eps = samples_out[sample] * (np.abs(samples_out[sample])**2 - r_tmp)                         
                mu = mu_rde
            
            # ...and update impulse response, if necessary
            if (int(sample/sps) <= stop_adapting):
                # update impulse response, see [1], eq (28)
                h -= mu * np.conj(samples_in[n_taps+sample:sample:-1]) * eps
            
            # save return info, if necessary
            if return_info:                
                # h_tmp.append(h.copy())
                # eps_tmp.append(eps)
                h_tmp[n_update,:] = h.copy()
                eps_tmp[n_update] = eps
            
            n_update += 1
                
    return samples_out, h_tmp, eps_tmp

def blind_adaptive_equalizer(sig, n_taps=111, mu_cma=5e-3, mu_rde=5e-3, mu_dde=0.5, decimate=False, 
                             return_info=True, stop_adapting=-1, start_rde=5000, 
                             start_dde=5000, exec_mode='auto'):    
    """
    Equalize the signal using a blind adaptive equalizer filter.
    
    A complex valued filter is initialized with a dirac impulse as impulse 
    response of length n_taps samples. Then the first signal sample is filtered.
    
    There exist tree operation modes:
        
        * constant modulus algorithm (CMA) operation [1]:
             Once each SYMBOL, the error (eps) to the desired output radius is calculated 
             and the filter impulse response is updated using the steepest gradient descent method [1]. 
             A step size parameter (mu_cma) is used to determine the adaptation speed. 
        * radially directed equalizer (RDE) operation [2]:
            Once each SYMBOL, the output signal is decided to ONE  of the desired radii (the nearest one) [2] and [3].
            The error (eps) between the output signal and this decided radius is calculated 
            and the filter impulse response is updated using the steepest gradient descent method. 
            A step size parameter (mu_rde) is used to determine the adaptation speed. 
        * decision directed equalizer (DDE) [2]:
            Once each SYMBOL, the output signal is decided to ONE, the nearest constellation point.
            The error (eps) between the output signal and this decided constellation point is calculated 
            and the filter impulse response is updated using the steepest gradient descent method. 
            A step size parameter (mu_dde) is used to determine the adaptation speed. CAUTION: this option
            works only very unreliable in case of phase noise!!!
            
    All three modes are in general run sequentially in the order CMA, RDE and DDE. 
    The parameters start_rde and start_dde control when the modes are switched.
    start_rde defines after how many SYMBOLS the RDE mode ist started after the 
    start of CMA mode. start_rde=0 does not start this mode at all.
    
    start_dde defines after how many SYMBOLS the DDE mode is started AFTER the 
    RDE mode has started. However, if the RDE mode is switched off (start_rde=0)
    the parameter start_dde defindes after how many SYMBOLS the DDE mode is 
    started AFTER the CMA mode has started. start_dde=0 does not start this mode at all.
    
    EXAMPLES:
    
        * start_rde=10e3, start_dde=0     -->     10e3 symbols CMA, rest RDE
        * start_rde=0, start_dde=0        -->     all samples filterd with CMA
        * start_rde=0, start_dde=10e3     -->    10e3 symbols CMA, rest DDE
        * start_rde=0, start_dde=1        -->    1 symbol CMA, rest DDE
        * start_rde=5e3, start_dde=10e3   -->     5e3 symbols CMA, 10e3 sybmosl RDE, rest DDE
    
    The equalizer can output every filtered SAMPLE or only every filtered 
    SYMBOL, which is controlled with the parameter decimate. 
    
    Further, the adaptation of the impulse response / equalizer can be stopped after 
    stop_adapting SYMBOLS.
    
    The equalizer operates on each signal dimension independently. The parameters
    can be passed as 
    
    * singular values [int, float or bool], which are broadcasted to every dimension, or
    * lists of length n_dims to specify independent parameters for each signal dimension
       
        
    Parameters
    ----------
    sig : skcomm.signal.Signal
        input signal to be equalized.
    n_taps : int or list of ints, optional
        length of the equalizers impulse response in samples for each dimension. Has to be odd. 
        The default is 111.
    mu_cma : float or list of floats, optional
        adaptation step size of the steepest gradient descent method for CMA operation for each dimension. 
        The default is 5e-3.
    mu_rde : float or list of floats, optional
        adaptation step size of the steepest gradient descent method for RDE operation for each dimension. 
        The default is 5e-3.
    mu_dde : float or list of floats, optional
        adaptation step size of the steepest gradient descent method for DDE operation for each dimension. 
        The default is 0.5.
    decimate : bool or list of bools, optional
        output every SAMPLE or every SYMBOL. The default is False.
    return_info : bool or list of bools, optional
        should the evolution of the impulse response and the error be recorded 
        and returned. The default is True.
    stop_adapting : int or list of ints, optional
        equaliter adaptation is stopped after stop_adapting SYMBOLS. -1 results 
        in a continous adaptation untill the last symbol of the input signal. 
        The default is -1.
    start_rde :  int or list of ints, optional
        defines after how many CMA SYMBOLS the RDE mode is started. start_rde=0 means 
        RDE mode does not start at all. See also examples above. The default is 5000.
    start_dde :  int or list of ints, optional
        defines after how many RDE SYMBOLS the DDE mode is started. start_rde=0 means 
        DDE mode does not start at all. See also examples above. The default is 5000.
    exec_mode: string
        Controls if Python or os-specific Cython implementation is used.
        'auto': use Cython module if available, otherwise use Python implementation.
        'python': force to use Python implementation, even if a Cython module is available.
        'cython': force to use the Cython module, might cause an error if Cython module is not available.
        The default is 'auto'.
    
    Returns
    -------
    results :  dict containing following keys
        sig : skcomm.signal.Signal
            equalized output signal.
        h : list of np.arrays 
            each list element consists of either a np.array of shape 
            (n_output_samples, n_taps) in case of return_info==True which
            documents the evolution of the equalizers impulse response or of an
            empty np.array in case of return_info==False
        eps : list of np.arrays
            each list element consists of either a np.array of shape 
            (n_output_samples,) in case of return_info==True which
            documents the evolution of the error signal or of an
            empty np.array in case of return_info==False.
            
    References
    ----------
    [1] D. Godard, “Self-recovering equalization and carrier tracking in twodimensional data communication systems,” IEEE Trans. Commun., vol. 28, no. 11, pp. 1867–1875, Nov. 1980.
    
    [2] S. Savory, "Digital Coherent Optical Receivers: Algorithms and Subsystems", IEEE STQE, vol 16, no. 5, 2010
    
    [3] P. Winzer, A. Gnauck, C. Doerr, M. Magarini, and L. Buhl, “Spectrally efficient long-haul optical networking using 112-Gb/s polarizationmultiplexed16-QAM,” J. Lightw. Technol., vol. 28, no. 4, pp. 547–556, Feb. 15, 2010.
    """

    if exec_mode=='auto':
        if cython_available:
            exec_mode = 'cython'
        else:
            exec_mode = 'python'

    if not isinstance(sig, signal.Signal):
        raise TypeError("input parameter must be of type 'skcomm.signal.Signal'")
    
    # parameter cheching
    if isinstance(n_taps,list):
        if len(n_taps) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        n_taps = [n_taps] * sig.n_dims
        
    if isinstance(mu_cma,list):
        if len(mu_cma) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        mu_cma = [mu_cma] * sig.n_dims
    
    if isinstance(mu_rde,list):
        if len(mu_rde) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        mu_rde = [mu_rde] * sig.n_dims
        
    if isinstance(mu_dde,list):
        if len(mu_dde) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        mu_dde = [mu_dde] * sig.n_dims
        
    if isinstance(decimate,list):
        if len(decimate) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        decimate = [decimate] * sig.n_dims
        
    if isinstance(return_info,list):
        if len(return_info) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        return_info = [return_info] * sig.n_dims
        
    if isinstance(stop_adapting,list):
        if len(stop_adapting) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        stop_adapting = [stop_adapting] * sig.n_dims
        
    if isinstance(start_rde,list):
        if len(start_rde) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        start_rde = [start_rde] * sig.n_dims
        
    if isinstance(start_dde,list):
        if len(start_dde) != sig.n_dims:
            raise ValueError('if parameters are given as lists, their length has to match the number of dimensions of the signal (n_dims)')
    else:
        start_dde = [start_dde] * sig.n_dims
    
    # generate lists for return info (evolution of impulse response and eps)
    h_tmp = []
    eps_tmp = []
    
    # iterate over dimensions
    for dim in range(sig.n_dims):        
    
        if n_taps[dim] % 2 == 0:
            raise ValueError('n_taps need to be odd')       
        
        # samples per symbol 
        sps = int(sig.sample_rate[dim] / sig.symbol_rate[dim])
        
        # init equalizer impulse response to delta
        # TODO: add option to set impulse response from outside
        h = np.zeros(n_taps[dim], dtype=np.complex128)
        h[n_taps[dim]//2] = 1.0
        
        # generate sample arrays
        samples_in = sig.samples[dim]
        samples_out = np.full(samples_in.size-n_taps[dim], np.nan, dtype=np.complex128)        
        
        # desired modulus for CMA for p=2, see [1], eq. (28) + 1    
        r = np.mean(np.abs(sig.constellation[dim])**4) / np.mean(np.abs(sig.constellation[dim])**2) 
        
        # desired radii for RDE, enhancement of [2], eq. (44)
        if start_rde[dim] > 0:
            radii = np.unique(np.abs(sig.constellation[dim]))**2
        else:
            radii = np.asarray([0.0])
        
        # convert symbols to samples
        start_rde[dim] = start_rde[dim] * sps
        start_dde[dim] = start_dde[dim] * sps
        # calc number of CMA symbols               
        if start_rde[dim] == start_dde[dim] == 0:
            n_CMA = samples_out.size
        else:
            n_CMA = start_rde[dim] if start_rde[dim] != 0 else start_dde[dim]
        # calc number of DDE symbols:
        n_DDE = 0 if start_dde[dim] == 0 else samples_out.size - (n_CMA + start_dde[dim])
        n_DDE = n_DDE if start_rde[dim] != 0 else samples_out.size - (n_CMA)
        # calc number of RDE symbols
        n_RDE = samples_out.size - (n_CMA + n_DDE)
       
        # # DEBUG PRINTS
        # skc_log.debug('samples_out:{}, start_rde: {}, start_dde:{}'.format(samples_out.size, start_rde[dim], start_dde[dim]))
        # skc_log.debug('n_CMA:{}, n_RDE: {}, n_DDE:{}\n'.format(n_CMA, n_RDE, n_DDE))
            
        # is output caluculated for each sample or only for each symbol?
        if decimate[dim]:
            shift = sps
        else:
            shift = 1
        # will the equalizer stop adapting
        if stop_adapting[dim] == -1:
            stop_adapting[dim] = int(samples_out.size/sps)
        
        # generate new list element for each dimension and initialize return values
        if return_info[dim]:            
            h_tmp.append(np.full((int(np.ceil((samples_in.size-n_taps[dim])/shift)), n_taps[dim]), 
                                 np.nan, dtype=np.complex128())) 
            eps_tmp.append(np.full(int(np.ceil((samples_in.size-n_taps[dim])/shift)), 
                                   np.nan, dtype=np.complex128()))
        else:
            h_tmp.append(np.asarray([[]],dtype=np.complex128()))
            eps_tmp.append(np.asarray([],dtype=np.complex128()))
        
        # use compiled Cython code for EQ loop...
        if exec_mode=='cython':
            samples_out, h_tmp[dim], eps_tmp[dim] = rx_cython._bae_loop(samples_in, 
                                                                        samples_out, 
                                                                        h, n_taps[dim], 
                                                                        sps, n_CMA, 
                                                                        mu_cma[dim], 
                                                                        n_RDE, mu_rde[dim], 
                                                                        radii, mu_dde[dim], 
                                                                        stop_adapting[dim], 
                                                                        sig.constellation[dim], 
                                                                        r, shift, return_info[dim], 
                                                                        h_tmp[dim], eps_tmp[dim])            
        # ... or use Python code
        elif exec_mode=='python':
            samples_out, h_tmp[dim], eps_tmp[dim] = _bae_loop(samples_in, samples_out, 
                                                              h, n_taps[dim], sps, 
                                                              n_CMA, mu_cma[dim], 
                                                              n_RDE, mu_rde[dim], 
                                                              radii, mu_dde[dim], 
                                                              stop_adapting[dim], 
                                                              sig.constellation[dim], 
                                                              r, shift, return_info[dim], 
                                                              h_tmp[dim], eps_tmp[dim])
        
        # only take "valid" (actually calculated) output samples
        if decimate[dim]:
            samples_out = samples_out[::sps]
            sig.sample_rate[dim] = sig.symbol_rate[dim]
        
        # generate output signal and return_info np.arrays
        sig.samples[dim] = samples_out
        
    # generate result dict
    results = dict()
    results['sig'] = sig
    results['h'] = h_tmp
    results['eps'] = eps_tmp
    return results

def combining(sig, comb_method='EGC', weights=None, combine_upto=None):
    """
    Performs Diversity Combining of the rows of a passed n-dimensional signal-
    class object, where each row represents the signal captured by an antenna 
    of a SIMO system, according to the passed SNR values per dimension 
    if comb_method == MRC. If comb_method == EGC, because of equal gain, there 
    is no need for know SNR of the signals.

    Parameters
    ----------
    sig : signal-class object
        n-dimensional signal object with list of sample arrays in the 'samples'
        attribute.
    comb_method : str, optional
        Combining method. MRC, EGC, and SDC are available. The default is 'MRC'.
    weigths : 1d numpy array, optional for comb_method == EGC
        array of weigthing values matching the number of signal dimensions of  
        the sig object for combination. 
    combine_upto : int, optional
        Combine just first n sample arrays togheter. If None, all sample arrays 
        are combined. Default is None.

    Returns
    -------
    sig_comb : signal object
        one-dimensional signal object after combining. The sample attribute now 
        has the combined sample array in the 'samples' attribute of its only 
        dimension.

    """
    # error checks
    if len(sig.samples) < 2:
        skc_log.error("Signal object only has one dimension. No combining was performed.")
        return sig

    if comb_method == "MRC":
        weigths = np.array(weights)
        if sig.n_dims != weigths.size:
            raise ValueError("Number of signal dimensions must match length of SNR value array.")
        
    # create new object with one dimension
    sig_comb = signal.Signal(n_dims=1)
    for key in vars(sig):
        if key == '_n_dims':
            pass
        else:
            vars(sig_comb)[key] = vars(sig)[key][0]
    
    # scaling           
    if comb_method == 'MRC':
        for i in range(len(sig.samples)):
            sig.samples[i] = sig.samples[i] * weights[i] 
    elif comb_method == 'EGC':
        pass
    elif comb_method == 'SDC':
        mask = np.where(weights == np.max(weights),1,0)
        for i in range(len(sig.samples)):
            sig.samples[i] = sig.samples[i] * mask[i]
    else:
        raise ValueError("Combining method not implemented. Available options are MRC, EGC, and SDC.")
        
    if combine_upto is not None:
        combine_range = int(combine_upto)
    else: 
        combine_range = len(sig.samples)

    # combination
    sig_comb.samples = np.sum(sig.samples[:combine_range],axis=0)
    # normalize samples to mean power of 1
    sig_comb.samples = sig_comb.samples[0] / (np.sqrt(np.mean(np.abs(sig_comb.samples[0])**2)))
    
    return sig_comb

def frequency_offset_correction(samples, sample_rate=1.0, symbol_rate=1.0, matched_filter=None, roll_off=None, max_FO_expected=None, debug=False):
    """
    Frequency offset estimation and correction (FOE and FOC).
    
    This function estimates the carrier frequency offset by using a spectral
    method described in [1]. The spectral method is performed on symbols, therefore 
    a decimation is applied in case of oversampling.

    If specified, the incoming samples are filtered with a matched filter before they are raised to
    the power of 4. FFT is formed afterwards. If specified, the search range for the frequency offset is
    is limited to +-max_FO_expected. 

    For a more accurate estimation, a 2nd order polynomial is fitted to the spectrum before the maximum is determined. 
    The correction is done by multiplying the samples with a complex exponential having a frequency with same amplitude
    as the estimated frequency offset, but opposite sign.        

    Please note that only singals with an integer oversampling factor can currently be handled.
    
    Parameters
    ----------
    samples : 1D numpy array, real or complex
        input signal. It is assumed that the first sample is taken at the correct (temporal) 
        sampling instant, theefore no timing recovery needs to be performed.   
    sample_rate : float
        sample rate of input signal in Hz. The default is 1.0.
    symbol_rate : float
        symbol rate of input signal in Hz. The default is 1.0.
    matched_filter : string
        if not None, matched filter specified by string is applied.
        Currently only a 'root raised cosine' filter ('rrc') is implemented. Default is None.
    roll_off : float
        roll off of the (matched) root raised cosine filter. Default is None.
    max_FO_expected : float
        if not None, search range of the frequency offset. The frequency offset is expected to 
        be in the range of +- max_FO_expected. Default is None.
    debug : bool
        Debug flag for generating debug plots. Default is False.

    Returns
    -------
    results:  dict containing following keys
        samples_corrected: 1D numpy array, real or complex
                            output signal, which is input signal multiplyied with a complex
                            exponential having a frequency with same amplitude
                            as the estimated frequency offset, but opposite sign.
        estimated_fo: 1D numpy array, real or complex. 
                    Estimated frequency offset in Hz.
                    
    References
    ----------
    [1] Savory, S. (2010, September/Oktober). Digital Coherent Optical Receivers: 
        Algorithms and Subsystems. Abgerufen von https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=5464309 
    
    [2] Dr. Lichtmann, M.. (o.J.). PySDR: A Guide to SDR and SDP using Python. 
        Abgerufen von https://pysdr.org/content/frequency_domain.html
     
    """  
    # make copy of original samples (and sample rate) for applying FOC on original samples
    samples_orig = samples
    sample_rate_orig = sample_rate

    # (matched) filtering for increasing FOC performance if specified. 
    if matched_filter is not None and matched_filter == "rrc":
        samples = filters.raised_cosine_filter(samples, sample_rate=sample_rate, symbol_rate=symbol_rate, 
                                         roll_off=roll_off, length=-1, root_raised=True, domain='freq')
    elif matched_filter is not None and matched_filter != "rrc":
        raise Exception("matched filter type for FOC not implemented or specified!")

    # decimate samples to 1 sps
    # for decimation, a integer oversamples (e.g. 2) is required. Otherwise, raise error
    if (sample_rate/symbol_rate)%1 != 0:
        raise Exception("integer oversampling or sps=1 required for FOC")
    samples = samples[::int(sample_rate/symbol_rate)]
    sample_rate = symbol_rate # sample rate is now symbol rate

    # Creating time and frequency axis
    t = np.arange(0, np.size(samples)) / sample_rate 
    f = np.fft.fftshift(np.fft.fftfreq(t.shape[-1], d=1/sample_rate)) 
  
    # samples to the power of order 4 and FFT
    order = 4
    samples_foe = samples**(order)
    raised_spectrum = np.fft.fftshift((np.abs(np.fft.fft(samples_foe))))
    
    # crop spectrum to expected FOE area if specified
    if max_FO_expected is not None:
        search_window = np.abs(f) <= max_FO_expected*order
        raised_spectrum *= search_window        

    # Finding Index of peak of power spectrum
    max_freq_index = np.argmax(raised_spectrum) 

    # Shift frequency to baseband for numerical better polyfit 
    shift_freq = f[max_freq_index]
    f = f-shift_freq

    # Polyfit 2nd order (range of polyfit: maximum +-1 sample)
    y_res = raised_spectrum[max_freq_index-1:max_freq_index+2]
    x_res = f[max_freq_index-1:max_freq_index+2]
    poly_coeffs = np.polyfit(x_res, y_res, 2)

    # Index of maximum of polyfit = x0 = -b/(2*a)
    max_value_of_poly = -poly_coeffs[1]/(2*poly_coeffs[0])

    # shift back to original value of peak of power spectrum
    f = f+shift_freq
    
    # Estimated Frequency offset
    estimated_freq = (max_value_of_poly+shift_freq)/order

    # Frequency Offset Recovery / compensation on original data
    t_orig = np.arange(0, np.size(samples_orig)) / sample_rate_orig 
    samples_corrected = samples_orig * np.exp(-1j*2*np.pi*(estimated_freq)*t_orig)    

    # generate plots for debugging if specified
    if debug:
        plt.figure()
        plt.title("FOE raised spectrum")
        plt.plot(f, 20*np.log10(raised_spectrum / np.max(raised_spectrum)), label="20*np.log(spectrum / np.max(spectrum))")
        plt.xlabel("frequency [Hz]")
        plt.grid(which="both")
        plt.legend()

        fbins_old = np.fft.fftshift((np.abs(np.fft.fft(samples_orig, n=int(t.shape[-1])))))
        fbins_new = np.fft.fftshift((np.abs(np.fft.fft(samples_corrected, n=int(t.shape[-1])))))

        plt.figure()
        plt.title("FOC raised spectrum(s)")
        plt.plot(f, 20*np.log10(fbins_old / np.max(raised_spectrum)), label="before FOE/FOC")
        plt.plot(f, 20*np.log10(fbins_new / np.max(raised_spectrum)), linestyle="--", label="after FOE/FOC")
        plt.xlabel("frequency [Hz]")
        plt.grid(which="both")
        plt.legend()

        plt.show()

    # generate output dict containing recoverd symbols and estimated frequency offset
    results = dict()
    results['samples_corrected'] = samples_corrected
    results['estimated_fo'] = estimated_freq
    return results

def stokesEQ_pdm(samples_X:np.ndarray, samples_Y:np.ndarray)->dict:
    """
    Performs blind polarization demultiplexing of polarization-multiplexd complex-modulated signals.
    The method is based on finding (in Stokes space) a symmetry plane of the incoming sampled data 
    (including symbol transitions) from a SVD-based least squares fit [1/sect.3.2].
    The normal vector of the plane identifies the two orthogonal input polarization states and is 
    used to determine the desired polarization-rotation of the input signal.The method does not
    involve data demodulation and thus should work independent of the modulation format.
    
    Parameters
    ----------
    samples_X:
        Input signal samples in X-polarization. Must be of same size as 'samples_Y'
    samples_Y:
        Input signal samples in Y-polarization. Must be of same size as 'samples_X'

    Returns
    -------

    results containing the following keys
        * samples_X : np.ndarray[np.complex128 | np.float64]
            Output signal samples after polarization separation in X-polarization.
        * samples_Y : np.ndarray[np.complex128 | np.float64]
            Output signal samples after polarization separation in Y-polarization.
        * U : np.ndarray[np.complex128 | np.float64]
            :math:`2\\times2` Jones (rotation) matrix :math:`\mathbf{U}`, which was applied to the input signal.
        * Nv : np.ndarray[np.float64]
            :math:`3\\times1` normal vector of the fitting plane in Stokes space (w/o centroid).
        * C : np.ndarray[np.float64]
            Centroid (:math:`3\\times1` average Stokes vector) of all Stokes space samples.
    
    References
    ----------
    .. [1] B. Szafraniec, B. Nebendahl, and T. Marshall, “Polarization demultiplexing in 
        Stokes space,” Optics Express, vol. 18, no. 17, pp. 17928–17939, Aug. 2010
        `(DOI) <https://opg.optica.org/oe/fulltext.cfm?uri=oe-18-17-17928&id=204878>`_

    See Also
    --------
    :meth:`skcomm.channel.rotatePol_pdm()`
    :meth:`skcomm.utils.jones2stokes_pdm()`
    """
    # TODO: add additional parameters: 
    # - range of used input samples [start, stop]
    # - flag for only providing estimation of rotation matrix (w/o changing actual PDM signal)
    # - option to fragmentize estimation and correction to implement a dynamic polarizatioh tracker
    # - debug option : Stokes-space plot of signal & fitted plane etc.
    # - add option for real-valued (or 1D) modulation formats, like BPSK, PAM, OOK

    # 0. Input argument checks
    if (not isinstance(samples_X,np.ndarray)) or (not isinstance(samples_Y,np.ndarray)) or (samples_X.ndim!=1) or (samples_Y.ndim!=1):
        raise TypeError('samples_X and samples_Y must be 1-D Numpy arrays')
    if samples_X.shape != samples_Y.shape:
        raise ValueError('samples_X and samples_Y must be of same size!')
    
    # TODO: assign a function parameter to select the range of input samples for estimation
    N = samples_X.size # temp: use all samples for estimation
    N = np.min((N,samples_X.size)) # no. of SOPs used for pol. estimation
    
    # 1. Convert PDM signal from Jones space to Stokes space (sample-wise)
    result_j2s = utils.jones2stokes_pdm(samples_X[:N],samples_Y[:N])
    _, S1, S2, S3 = result_j2s["S0"], result_j2s["S1"], result_j2s["S2"], result_j2s["S3"]
    # maxpow = np.max(np.abs(S0))

    # 2. Fit plane into Stokes space samples (in least squares sense):
    # https://math.stackexchange.com/questions/99299/best-fitting-plane-given-a-set-of-points
    X = np.vstack((S1,S2,S3))   # form a 3×N matrix X out of the resulting coordinates
    C = np.mean(X,axis=1) # subtract out the centroid
    X = X - C[:,np.newaxis]
    U,S,_ = np.linalg.svd(X,full_matrices=False) # calculate its singular value decomposition
    idx = np.argmin(S) # the normal vector of the best-fitting ellipsoid is the left singular vector corresponding to the SMALLEST singular value
    Nv = U[:,idx] # normal vector of best-fitting ellipsoid
    # print('\nAngles (deg) of normal vector of best-fitting plane:') # TODO: check!
    # print('phi_s1={0:.1f}°,  phi_s2={1:.1f}°,  phi_s3={2:.1f}°\n'.format(*np.arccos(Nv)*180/np.pi))

    # 3. Calculate from Nv the angles psi, phi and theta for the polarization-rotation function "rotatePol_pdm"
    # to rotate (in Jones space) the input samples into S2-S3 plane in Stokes space (i.e. perform polarization separation)
    # see https://de.mathworks.com/matlabcentral/answers/79296-how-to-do-a-3d-circle-in-matlab: answer by Kye Taylor
    # see https://en.wikipedia.org/wiki/Stokes_parameters
    # ==> see PhD diss D.Sperti, Appdx. A, Fig.A.1 ()
    Theta_2   = 0.5*np.arctan2(Nv[1], Nv[0]) # angle 2*Theta between S1-axis and the projecton of SOP-vector onto the S1-S2-plane
    Epsilon_2 = 0.5*np.arctan2(Nv[2], np.sqrt(Nv[0]**2 + Nv[1]**2)) # angle 2*Epsilon between SOP-vector and the S1-S2-plane

    # 4a. Rotate SOP-vector back around S3-axis: SOP now in plane S1-S3
    tmp = np.asarray([0.0]*3)
    result_rot = channel.rotatePol_pdm(tmp,tmp, theta=-Theta_2, phi=0.0, psi=0.0)
    U1 = result_rot["U"]
    
    # 4b. rotate back Epsilon around S2-axis: SOP gets parallel to S1 (i.e., pol. separated)
    result_rot = channel.rotatePol_pdm(tmp, tmp, theta=Epsilon_2, psi=0.0, phi=np.pi/2)
    U2 = result_rot["U"]

    # 5.matrix multiplication with input Jones vector (sample-by-sample)
    U = U2 @ U1 # combined rotation matrix (Jones space)
    output = U @ np.vstack((samples_X.flatten(),samples_Y.flatten()))

    results = {
    "samples_X": output[0],
    "samples_Y": output[1],
    "U": U,
    "Nv": Nv, # normal vector of fitting plane (w/o centroid)
    "C":  C # centroid (mean Stokes vector)
    }
    return results

def _da_frame_sync_loop(Q, samples, shift, search_range):
    """ Helper function which implements the actual loop for the data-aided frame synchronization (da_frame_sync).
    
    For explanation and help regarding the input and output parameters see
    docu of :meth:`skcomm.rx.da_frame_sync()` 
    """

    for d in np.arange(search_range):    
        R1 = samples[d:d+shift]
        R2 = samples[d+shift:d+2*shift]
        R3 = samples[d+2*shift:d+3*shift]
        R4 = samples[d+3*shift:d+4*shift]
        # see [1] eq. (14)
        P1 = np.dot(np.conj(R1),R2) - np.dot(np.conj(R2),R3) - np.dot(np.conj(R3),R4)
        P2 = np.dot(np.conj(R2),R4) - np.dot(np.conj(R1),R3)
        P3 = np.dot(np.conj(R1),R4)
        # see [1] eq. (15)
        R_i_squared = np.dot(R1,np.conj(R1)) + np.dot(R2,np.conj(R2)) + np.dot(R3,np.conj(R3)) + np.dot(R4,np.conj(R4))
        R_i_squared = np.real(R_i_squared)
        Q[d] = (np.abs(P1) + np.abs(P2) + np.abs(P3)) / (1.5*R_i_squared)
    
    return Q

def da_frame_sync(samples:np.ndarray[np.complex128 | np.float64], shift:int=64, search_range:int=1000, 
                  dbg_plot:bool=False, fNum:int=1, exec_mode:str='auto')->np.int64:    
    """
    Perform temporal synchronization to a reference frame.

    Perform temporal synchronization of the received signal samples to a known training sequence as suggested in [1].
    The training sequence is a random sequence (denoted as 'B') which is repeated four times, while the third repetition
    is negated ('[B,B,-B,B]').

    References
    ----------
    [1] Shi et al. "Coarse Frame and Carrier Synchronization of OFDM Systems: A New Metric and Comparison", IEEE Transactions on wirless communiations vol 3, no. 4 2004)

    Parameters
    ----------
    samples
        samples of the received signal.
    shift
        length of the repeated random sequence (length of 'B') including oversampling, i.e. repetition period (in samples) of the sequence.
    search_range
        specifies the range in samples whithin the signal is searched for the frame sync header. Can be shortened to save processing time.    
    dbg_plot
        Should a plot of the calculated frame sync metric be generated? Helpful for debugging purpose.
    fNum
        Figure number of the debut plot graph.
    exec_mode
        Controls if Python or os-specific Cython implementation is used. 
        'auto': use Cython module if available, otherwise use Python implementation.
        'python': force to use Python implementation, even if a Cython module is available.
        'cython': force to use the Cython module, might cause an error if Cython module is not available.
        The default is 'auto'.

    Returns
    -------
        Estimated shift (in samples) which has to be applied to the received signal to be temporally synchronized to the frame synchronization sequence.
    
    See Also
    --------
    :meth:`skcomm.tx.insert_frame_sync_seq()`    
    
    """

    if samples.size < shift*4+search_range:
        skc_log.warning(f'search range plus shift ({shift*4+search_range}) is larger than size of given sampled signal ({samples.size}). Aborting and return zero estimated shift!')
        return 0

    if exec_mode=='auto':
        if cython_available:
            exec_mode = 'cython'
        else:
            exec_mode = 'python'

    # calc metric    
    Q = np.zeros(search_range)

    if exec_mode.lower() == 'cython':
        Q = rx_cython._da_frame_sync_loop(Q, samples, shift, search_range)
    elif exec_mode.lower() == 'python':
        Q = _da_frame_sync_loop(Q, samples, shift, search_range)  

    est_shift = np.argmax(Q)    

    if dbg_plot:
        plt.figure(fNum)
        plt.figure(fNum).clear()        
        plt.plot(Q)
        plt.title(f'Frame sync metric: estimated delay {est_shift}')

    return est_shift

def da_cfo_estimation(samples:np.ndarray[np.complex128], shift:int=64, sample_rate:float=1.0)->float:
    """
    Estimate carrier frequency offset.

    Estimate the amount of (residual) carrier frequency offset (CFO) of the received signal samples by using 
    a known training sequences as suggested in [1]. The training sequence is a random sequence (denoted as 'B') 
    which is repeated four times, while the third repetition is negated ('[B,B,-B,B]').
    
    References
    ----------
    [1] Shi et al. "Coarse Frame and Carrier Synchronization of OFDM Systems: A New Metric and Comparison", IEEE Transactions on wirless communiations vol 3, no. 4 2004

    Parameters
    ----------
    samples
        samples of the received signal.
    shift
        length of the individual random sequence which was reapeted four times (i.e. length of 'B') including oversampling, 
        i.e. repetition period (in samples) of the sequence.
    sample_rate
        sample rate of the received sampled signal.  

    Returns
    -------
        Estimated CFO in Hertz.  

    See Also
    --------
    :meth:`skcomm.tx.insert_frame_sync_seq()`   
    """

    # estimate CFO
    R1 = samples[:shift]
    R2 = samples[shift:2*shift]
    R3 = samples[2*shift:3*shift]
    R4 = samples[3*shift:4*shift]

    # see [1], eq. (14)
    P1 = np.dot(np.conj(R1),R2) - np.dot(np.conj(R2),R3) - np.dot(np.conj(R3),R4)
    # see [1], eq. (17)
    est_eps = np.angle(P1) * 2 / np.pi
    # calc absolute carrier frequency offset from normalized offset estimate (eps)
    est_cfo = est_eps / (shift*4) * sample_rate    

    return est_cfo

def da_snr_estimation(ce_header_rx:np.ndarray[np.complex128], pilot_pos:np.ndarray[int], n_fft:int,
                      n_repeat:int, pilot_neighbors:int=3, windowed:bool=False, bw_limit:float=0.5,
                      dbg_plot:bool=False, fNum:int|None=None)->dict:
    """ Estimates the SNR of a received signal using repeated OFDM pilot symbols in the channel estimation headers.

    The signal-to-noise ratio (SNR) of the received signal is measured using known OFDM channel estimation 
    (ce) header sequences `ce_header_rx`, that are available in all signal dimensions. It is assumed that
    such sequences were inserted into all transmit signal dimensions with  the method :meth:`skcomm.tx.insert_ch_est_seq()`.

    The method utilizes the property that, when an OFDM symbol is repeated `n_repeat` times in time domain, 
    it results in a discrete spectrum in frequency domain with a `n_repeat` times higher frequency resolution
    and with spectral gaps of width (`n_repeat`-1) between the 'original' OFDM pilot tones. These gaps are used
    here for an in-band estimation of the noise power, as they are filled (mainly) by additive channel noise.

    The signal+noise power (`p_sig_and_n`) is estimated by adding over all frequency bins in the squared-magnitude
    spectrum of `ce_header_rx` that are attributed to OFDM pilots as given in the list `pilot_pos`, which was
    defined for the ce-header generation function  :meth:`skcomm.tx.insert_ch_est_seq()`. Also, the frequency
    images of the OFDM pilots, that arise from the assumed 2-times oversampling, are included in the measurement
    of `p_sig_and_n`.
    In addition, symmetrically around each of these pilot positions, :math:`\pm` `pilot_neighbors`
    frequency bins can be included in order to increase the robustness against carrier-frequency offset (CFO).
    Alternatively to using `pilot_neighbors`, a time-domain windowing (Blackmann-Harris) can be applied to the
    `ce_header_rx` to increase the CFO robustness.

    The noise power spectral density (PSD) `p_n_per_bin` (noise power per frequency bin) is estimated by averaging 
    the power over all frequency bins in the squared-magnitude spectrum of `ce_header_rx` that are not attributed
    for the estimation of `p_sig_and_n` and at least `pilot_neighbors`+1 bins away from pilot-tone bin positions.

    Finally, the `SNR per symbol` (in `dB`) is calculated as :math:`\mathrm{SNR} = 10\cdot\mathrm{log}_{10}(P_{sig}/P_{noise})`,
    where :math:`P_{sig}` is the signal+noise power corrected for the noise power within the frequency range
    used for measuring `p_sig_and_n`, and :math:`P_{noise}` is the noise PSD `p_n_per_bin` accumulated over
    a bandwidth that equals the `symbolrate` (= 0.5 * `samplerate`).

    .. warning:: This method strictly assumes a samplerate of 2 samples/symbol for the channel estimation header
     and will lead to incorrect results if otherwise!

    Parameters
    ----------
    ce_header_rx
        The received channel estimation (ce) header (incl. 2-times oversampling) as generated in the transmitter
        by the method :meth:`skcomm.tx.insert_ch_est_seq()`.
    pilot_pos
        Indices of frequency bins containing OFDM pilot tones (as inserted into the transmit signal by the method
        :meth:`skcomm.tx.insert_ch_est_seq()`).
    n_fft
        Size of the FFT used for generating the ce-header (as inserted into the signal by the method
        :meth:`skcomm.tx.insert_ch_est_seq`.
    n_repeat
        Number of repetitions of the OFDM pilot symbol in the applied `ce_header_rx`. If OFDM guard intervals were
        eventually removed in the reciver, this number can be smaller than the number of repetitions originally used
        in the method :meth:`skcomm.tx.insert_ch_est_seq()` at the transmitter.
    pilot_neighbors
        Number of frequency bins on each side of OFDM pilots which are included in the signal-power measurement and
        excluded from the noise-power measurement. By adding the OFDM-pilot power across multiple frequency bins,
        the robustness against carrier frequency offset (CFO) can be increased. The default is pilot_neighbors=3.
    windowed
        If set to True, a Blackmann-Harris window is applied to the `ce_header_rx` in time-domain before calculating
        its FFT. This method can be used alternatively to `pilot_neighbors` to improve CFO robustness.
    bw_limit
        A single-sided bandwidth limit (normalized to the `samplerate`) that is applied to the signal and noise bins in
        order to limit the SNR estimation to a frequency range occupied by the signal. The parameter ranges from 
        bw_limit=0.25 to bw_limit=0.5 (default), where 0.5 corresponds to the frequency range `-symbolrate` ... `symbolrate`.
    dbg_plot
        If set to True, the FFT of the channel estimation header and the markers for power and noise measurements
        are plotted.
    fNum
        Figure number for the debug plot window. The default is None, which uses the "next unused figure number".

    Returns
    -------
       dictionary with following keys

        * 'p_sig' : np.ndarray[np.complex128 | np.float64] 
            Estimated signal power (linear units).
        * 'p_n'  : np.ndarray[np.complex128 | np.float64] 
            Estimated noise power within the `symbolrate` (linear units).
        * 'snr_dB' : float 
            Estimated SNR per symbol in `dB`.
            
    See Also
    --------
    :meth:`skcomm.tx.insert_ch_est_seq()`  
    """

    # input checks
    if (type(n_fft) is not int) or (n_fft <= 2) or (n_fft%2 != 0):
        raise ValueError("n_fft must be an even integer > 2")
    
    if (type(n_repeat) is not int) or (n_repeat < 2): # or (n_repeat%2 != 0):
        raise ValueError("n_repeat must be an integer >= 2")
    
    if (type(ce_header_rx) is not np.ndarray) or (ce_header_rx.ndim != 1) or (ce_header_rx.size != 2*n_fft*n_repeat):
        raise TypeError("ce_header_rx must be a 1D numpy.ndarray of size  2*n_fft*n_repeat")
    
    if (type(pilot_pos) is not np.ndarray) or (pilot_pos.ndim != 1) or (pilot_pos.size == 0) or (pilot_pos.size > n_fft):
        raise TypeError("pilot_pos must be a 1D numpy.ndarray with 0 < pilot_pos.size <= n_fft")
    pilot_pos = np.sort(np.unique(pilot_pos.copy()))
    
    if (type(pilot_neighbors) is not int) or (pilot_neighbors < 0): # or (n_repeat - 2*pilot_neighbors < 2):
        raise ValueError("pilot_neighbors must be an non-negative integer") # <= floor(n_repeat/2-1)")

    if (type(windowed) is not bool):
         raise TypeError("windowed must be of Boolean type")
    
    if (type(bw_limit) is not float) or (bw_limit<0.25) or (bw_limit>0.5):
        raise ValueError("bw_limit must be a float with 0 < bw_limit <= 0.5")
    
    if (type(dbg_plot) is not bool):
        raise TypeError("dbg_plot must be of Boolean type")
    
    if (fNum is not None) and (type(fNum) is not int):
        raise TypeError("fNum must be None or an integer")
    
    # function works CORRECTLY ONLY with 2 samples per symbol (oversampling=2) !
    ce_header_size = ce_header_rx.size

    # limit max. number of `pilot_neighbors` so at least one freq. bin for noise measurement remains between OFDM pilot tones
    pilot_neigh = np.min((pilot_neighbors,n_repeat/2-1)).__int__()

    if windowed: # increase robustness against CFO with time domain windowing
        window_fct = 'blackmanharris' #'blackmanharris', 'flattop', ('kaiser',9)
    else: # w/o windowing, robustness against CFO can be increased with pilot_neighbors>0 (summing OFDM-pilot power over multiple frequency bins)
        window_fct = 'boxcar'  # boxcar = rectangular = no windowing
    
    window = ssignal.windows.get_window(window=window_fct, Nx=ce_header_size,  fftbins=False)
    PWR_SCALING = (window.sum()/window.size)**2 # power scaling (signal and noise power are equally reduced by this factor due to windowing)
    NENBW = window.size * (window**2).sum() / (window.sum())**2 # Normalized Equivalent Noise BandWidth of the window (in units of frequency bins),
                                                                # see eq.(21) in Heinzel et al Feb. 2002: "Spectrum and spectral density estimation by the DFT..." 
                                                                # and eq.(4.46) in van Etten 2005 "Introduction to Random Signals and Noise"

    ce_header_fft = np.fft.fft(window*ce_header_rx) / ce_header_rx.size # FFT spectrum of (windowed) ce_header
    fftfreqs = np.fft.fftfreq(ce_header_size) #  frequencies in range [0, ..., +0.5-1/N, -0.5, ..., -1/N] (assuming normalized samplerate Fs=1)

    # 2x oversampling (at Tx) results in repeated (and pulseshaped/filtered) spectrum -> pilot tones appear repeated in frequency domain (at `pilot_pos` + `n_fft`).
    # Repetition of header sequence in time domain results in increaded frequency resolution by a factor of `n_repeat`, i.e. gaps of length  `n_repeat`
    # filled with noise (and possibly some CFO and phase-noise leakage power).
    pilot_pos_up  = np.unique(np.concatenate((pilot_pos, pilot_pos+n_fft))).astype(int) * n_repeat

    if window_fct == 'boxcar': # include neighbor bins symetrically around original pilots for s+n power measurement (increasing CFO robustness; only needed for boxcar)
        pilot_idxs = np.unique( (np.tile(pilot_pos_up,(1+2*pilot_neigh,1))+np.arange(-pilot_neigh,pilot_neigh+1)[:,np.newaxis]).flatten() % ce_header_size)
    else:
        pilot_idxs = pilot_pos_up # for a windowed ce-header; use only one freq. bin for s+n power measurement

    # single-sided bandwidth limit (normalized to samplerate) to limit the SNR measurement range ==> scaling only valid for oversampling=2 !
    bw_limit = np.clip(bw_limit, 0.25, 0.5)  # restrict bw-limit frequency range from symbol-rate (roll-off=0) ... 2*symbol-rate (roll-off=1) 
    bw_limit_mask = np.argwhere((-bw_limit<=fftfreqs) & (fftfreqs<=bw_limit) ).flatten()

    # sum up (signal+noise)-power over all OFDM pilot positions within bw_limit
    p_sig_and_n_idxs = np.unique(np.asarray(list( set.intersection(set(bw_limit_mask),set(pilot_idxs)))))
    p_sig_and_n = np.sum(np.abs(ce_header_fft[p_sig_and_n_idxs])**2) / PWR_SCALING # corrected for power loss from windowing

     # +/-`noise_guard_bins` around pilot bins to exclude from noise measurement
    noise_guard_bins = pilot_neigh # always set to pilot_neigh
    noise_guard_mask = np.unique( (np.tile(pilot_pos_up,(1+2*noise_guard_bins,1))+np.arange(-noise_guard_bins,noise_guard_bins+1)[:,np.newaxis]).flatten() % ce_header_size)
    noise_idxs =  np.unique(np.asarray(list(set.intersection(set(bw_limit_mask), set(np.arange(ce_header_size))-set(noise_guard_mask)-set(p_sig_and_n_idxs))), dtype=np.int64) % ce_header_size)

    # calc noise power per frequency bin (PSD)
    p_n_per_bin = np.mean(np.abs(ce_header_fft[noise_idxs])**2) / PWR_SCALING / NENBW # (white) noise PSD, corrected for power loss from windowing and power gain from window equivalent noise bandwidth)
    p_n = p_n_per_bin * ce_header_size * 0.5 # -> sum up noise power in +/-symbol_rate/2 ==> only valid for oversampling=2 !
    
    # remove average noise power per bin from OFDM pilots
    # ! for very small signal powers, signal power might become negative due to statistical variations: => set signal power to (almost) 0 in these cases
    zeroPower = 1/np.finfo(p_sig_and_n.dtype).max
    p_sig = np.max([zeroPower, p_sig_and_n - p_sig_and_n_idxs.size*NENBW*p_n_per_bin])
    
    est_snr = 10*np.log10(p_sig/p_n)
    #print(f'{est_snr:.2f} dB')

     # debug plot
    if dbg_plot:
        if fNum:
            fig = plt.figure(fNum, facecolor='white', edgecolor='white')
            fig.clear()
        else:
            fig = plt.figure(facecolor='white', edgecolor='white')
        
        plt.plot(10*np.log10(np.abs(ce_header_fft)**2),'b-', label='ce_header spectrum')
        plt.plot(p_sig_and_n_idxs, 10*np.log10(np.abs(ce_header_fft[p_sig_and_n_idxs])**2),'sk', label='signal-power bins')
        plt.plot(noise_idxs, 10*np.log10(np.abs(ce_header_fft[noise_idxs])**2),'xr', label='noise-power bins')
        plt.axvline(ce_header_fft.size/2,ls=':',color="black", label=r'$-F_s/2$')

        plt.title(f'Estimated SNR using ce_header: {est_snr:.2f} dB')
        plt.xlabel('frequency-bin number')
        plt.ylabel("magnitude (dB)")
        plt.grid(visible=True)
        plt.legend()
        plt.show()

    skc_log.debug(f'estimated noise power at Rx (new): {p_n:.3e}')
    skc_log.debug(f'estimated signal power at Rx (new): {p_sig:.5f}')
    skc_log.debug(f'estimated SNR (new): {est_snr:.2f} dB')

    return {
        'p_sig' : p_sig,
        'p_n'   : p_n,
        'snr_dB': est_snr}

def da_channel_estimation(ce_header_rx_spec:np.ndarray[np.complex128], pilots:np.ndarray[np.complex128], 
                          pilot_pos:np.ndarray[np.int64], n_fft:int)->np.ndarray[np.complex128]:
    """ Estimate channel frequency resoponse.

    This method estimates the frequency response of the channel by comparing (dividing) the spectrum of known OFDM pilots within 
    received and sent channel estimation header sequences generated by the transmitter method :meth:`skcomm.tx.insert_ch_est_seq()`.

    .. warning:: This method assumes an oversampling of 2 of the channel estimation header and will lead to incorrect results in all other cases!

    Parameters
    ----------
    ce_header_rx_spec
        Spectrum of the received channnel estimation header sequence. The number of frequency bins (length of `ce_header_rx_spec`) needs to be `n_fft`*2.
    pilots
        Array of the actual sent OFDM pilot symbols (as inserted into the signal by method :meth:`skcomm.tx.insert_ch_est_seq()`).
    pilot_pos
        Array of the indices of the frequency bins of the sent OFDM pilot symbols (as inserted into the signal by method :meth:`skcomm.tx.insert_ch_est_seq()`).
    n_fft
        Size of the FFT used for generating the channel estimation header (as inserted into the signal by method :meth:`skcomm.tx.insert_ch_est_seq()`).

    Returns
    -------
        Estimated frequency response.

    See Also
    --------
    :meth:`skcomm.tx.insert_ch_est_seq()` 
    """
   
    # oversampling leads to repeating pilots in the frequency domain
    pilot_pos_up = np.concatenate((pilot_pos, pilot_pos+n_fft))
   
    # H = Y/X
    # sent pilots have to be repeated in the fequency domain as well
    H_est = ce_header_rx_spec[pilot_pos_up] / np.concatenate((pilots,pilots))
        
    return H_est

def calc_eq_frequency_response_zf(H:np.ndarray[np.complex128])->np.ndarray[np.complex128]:
    """ Calculate equalizer frequency response.

    Calculate the equalizer frequency response from a given / measured channel frequency response using the zero forcing approach.
    The given channel frequency response needs to be a three dimensional array where the first dimension specifies the receiver dimension (`n_rx` ),
    the second dimension specifies the transmitter dimension (`n_tx`) and the third dimension specifies the frequency index (`n`).

        * for the SISO case the array `H` should have the shape (`1,1,N`), where `N` is the number of frequency bins of the frequency response. In this case the equalizer frequency response is calculated according to 

            .. math:: W = H^*/|H|^2

            while :math:`()^*` means conjugate complex operation, :math:`/` means elementwise division and `||` means absolute value.

        * for the MIMO case the array `H` should have the shape (`n_rx,n_tx,N`), where `n_rx` and `n_tx` are the receiver and transmitter dimensions, respectively and `N` is the number of frequency bins of the frequency response. In this case the equalizer frequency response is calculated according to 
        
            .. math:: W(:,:,n) = (H(:,:,n)^HH(:,:,n))^{-1}H(:,:,n)^H

            while :math:`()^H` means Hermitian (conjugate and transpose) operation and :math:`()^{-1}` means matrix inversion
    
    Therefore, the equalizer frequency response array has a shape of (`n_tx,n_rx,N`).

    .. warning:: This method assumes an oversampling of 2 of the channel estimation header and will lead to incorrect results in all other cases!

    Notes about implementation: Because the function is expecting a signal with 2 samples per symbol, we have to deal with this oversampling also in calculating W. Assuming the SISO-case, we want to calculate
    :math:`H^{-1}`, which should produce a white spectrum AFTER downsampling. This means we do not expect a white spectrum before downsampling, but have to keep in mind overlapping spectra after downsampling
    due to expected roll-off of pulseshaping, which is done by the modulus-related term in the equotation(s).

    References
    ----------
    [1] M. Kuschnerov et al., "Data-Aided Versus Blind Single-Carrier Coherent Receivers," in IEEE Photonics Journal, vol. 2, no. 3, pp. 387-403, June 2010, doi: 10.1109/JPHOT.2010.2048308.

    [2] Reinhardt, Steffen "Einträgerübertragung mit Frequenzbereichsentzerrung - Erweiterung für Mehrantennensysteme, Synchronisationskonzepte und experimentelle Verifikation", PhD thesis, Friedrich-Alexander-Universität Erlangen-Nürnberg (FAU), 2007, https://open.fau.de/handle/openfau/396

    Parameters
    ----------
    H
        Channel frequency response of shape (n_rx,n_tx,N).

    Returns
    -------
    W
        Equalizer frequency response of shape (n_tx,n_rx,N).

    See Also
    --------
    :meth:`skcomm.rx.da_channel_estimation()`
    
    """
    
    # SISO case
    if H.ndim==3 and (H.shape[0]==1) and (H.shape[1]==1):
        # calc EQ transfer function (ZF) for SISO    
        # see [2] eq. (4.92),(4.93), (4.94)
        N = H.shape[2]
        W = np.zeros(H.shape, dtype=np.complex128) * np.nan
        for n in np.arange(N):
            W[:,:,n] = np.conj(H[:,:,n])/(np.abs(H[:,:,n])**2 + np.abs(H[:,:,int(np.mod(n+N/2,N))])**2)
    # SIMO case
    elif H.ndim==3 and (H.shape[0]>1) and (H.shape[1]==1):
        raise ValueError('SIMO case not implemented yet...')
    # MIMO case
    elif H.ndim==3 and (H.shape[0]>1) and (H.shape[1]>1):
        # calc EQ transfer function (ZF) for MIMO    
        # see [2], eq.  (4.92),(4.93), (4.94) + (5.49)
        # see [1], eq. (45)
        N = H.shape[2]
        W = np.zeros(np.transpose(H,axes=(1,0,2)).shape, dtype=np.complex128) * np.nan
        for n in np.arange(N):            
            H_n = H[:,:,n]
            H_n_H = np.conjugate(H_n.T)
            H_n_modN = H[:,:,int(np.mod(n+N/2,N))]
            H_n_modN_H = np.conjugate(H_n_modN.T)             
            W[:,:,n] = np.linalg.pinv(H_n_H @ H_n + H_n_modN_H @ H_n_modN) @ H_n_H            
    else:
        raise ValueError('transfer function H has wrong dimension or method not implemented')
    
    return W
    
def da_equalizer(sig_tx, sig_rx, cfo_comp:bool=False, decimate:bool=False, 
                 dbg_plt:bool=False, dbg_frame:int=-1, dbg_dim:int=-1)->dict:
    """ Data-aided equalizer.

    Equalize a signal by using given known training sequences. This method bundles multiple data-aided receiver function 
    as a wrapper and iterates over N frames, each consisting of 
    
        * a frame synchronization header (generated by :meth:`skcomm.tx.insert_frame_sync_seq()`)
        * a channel estimation header and (generated by :meth:`skcomm.tx.insert_ch_est_seq()`)
        * payload data.

    The steps performed within this function are:

        * frame synchronization for all rx_dims (using :meth:`skcomm.rx.da_frame_sync()`)
        * coarse CFO estimation and correction for all rx_dims and frames (using :meth:`skcomm.rx.da_cfo_estimation()`)
        * fine CFO for each frame individually (using :meth:`skcomm.rx.da_cfo_estimation()`)
        * SNR estimation for each frame and rx_dim individually (using :meth:`skcomm.rx.da_snr_estimation()`)
        * (MIMO) channel estimation for each frame individually (using :meth:`skcomm.rx.da_channel_estimation()`)
        * (MIMO) channel equalization for each frame individually (using :meth:`skcomm.rx.calc_eq_frequency_response()`)
        * removal of frame sync and channel estimatin headers
        * optional: decimation to one sample per symbol

    .. warning:: This method assumes an oversampling of 2 of the channel estimation header and will lead to incorrect results in all other cases!

    Parameters
    ----------
    sig_tx : skc.signal.Signal
        Sent signal.
    sig_rx : skc.signal.Signal
        Received signal.
    cfo_comp
        Carrier frequency offset estimation and correction active? For details see :meth:`skcomm.rx.da_cfo_estimation()`    
    decimate
        Decimation (downsampling) to one sample per symbol after equalization?
    dbg_plt
        Show debug plots?
    dbg_frame
        Which frame should be shown in debug plots? Only usable in case of `dbg_plt`=True.
    dbg_dim
        Which rx_dimension should be shown in debug plots? Only usable in case of `dbg_plt`=True. A value of -1 shows the last receiver dimension.

    Returns
    -------
        results with following keys

        * 'sig_rx' : skc.signal.Signal
            Equalized (and decimated) signal.
        * 'W_out'  : list of np.ndarray[np.complex128 | np.float64] 
            Equalizer frequency respnses for each individual processed frame. List of length n_frames, each element containing a three dimensional
            ndarray of shape (# Tx dims,# Rx dims,# frequency bins) representing the (MIMO) equalizer frequency responses.
        * 'H_out' : list of np.ndarray[np.complex128 | np.float64] 
            Estimated channel frequency respnses for each individual processed frame. List of length n_frames, each element containing a three dimensional
            ndarray of shape (# Rx dims,# Tx dims,# frequency bins) representing the (MIMO) frequency responses.
        * 'CFO_out' : list of np.float64
            Estimated carrier frequency offsets (coarse+fine) for each individual processed frame. List of length n_frames.
        * 'snr_dB_out' : list of np.float64
            Estimated SNR in dB for each individual processed frame. List of length n_frames. 

    See Also
    --------
    :meth:`skcomm.rx.da_frame_sync()`
    :meth:`skcomm.rx.da_cfo_estimation()`
    :meth:`skcomm.rx.da_snr_estimation()`
    :meth:`skcomm.rx.da_channel_estimation()`
    :meth:`skcomm.rx.calc_eq_frequency_response_zf()`
    """
     
    for dim in np.arange(sig_rx.n_dims):
        if sig_rx.sample_rate[dim]/sig_rx.symbol_rate[dim] != 2.0:
            raise ValueError('Only implemented for 2 samples per symbol!')
        
    # TODO:
    #   * add checks to ensure n_frames, data_len, header structure, etc. are equal for all receveived dims       
    
    oversampling = 2

    # get header, payload and frame lengths from first dim --> assumes that these parameters are equal for all dims
    fs_header_len = sig_rx.info[0]['fs_header']['length']
    ce_header_len = sig_rx.info[0]['ce_header']['length']
    header_len = fs_header_len + ce_header_len
    payload_len = sig_rx.symbols[0].size
    cpe_pilot_len = sig_rx.info[0]['cpe_pilot_info']['cpe_pilots'].size
    frame_len = header_len + payload_len + cpe_pilot_len

    # process more than one frame (start of header of next frame) in order to avoid filter effects from equalization and to be able
    # to find frame phase jumps (see calculation of frame_phase_diff and phase_jumps below)
    frame_overlap = 1000 # in samples (might change in last frame)

    # phases of first 'frame_phase_average' samples are averaged to calc an estimate of the phase at the beginning of the header in order to avoid phase jumps (see below)
    # more average -> better noise performance, but critical for larger CFO (or large CFO estimation errors)    
    frame_phase_average = 100 # in samples
    if frame_phase_average > frame_overlap:
        frame_phase_average = frame_overlap-1

    frame = 0
    start_sample = np.zeros(sig_rx.n_dims, dtype=np.int64)
    stop_sample = np.zeros(sig_rx.n_dims, dtype=np.int64)
    
    # output samples (for all frames) after equalization    
    samples_out = np.zeros((sig_tx.n_dims, 0), dtype=np.complex128)    
    # phase jumps between two consecutive frames after equalization (for individual dimensions)
    phase_jumps = np.zeros(sig_tx.n_dims)
    # stores the first 'frame_phase_average' samples of previously equalized frame to calc phase jumps
    first_samples_prev_frame = np.zeros((sig_tx.n_dims,frame_phase_average),dtype=np.complex128) * np.nan   
    # create variables to store EQ results for each frame individually
    W_out = []
    H_out = []
    CFO_out = []
    snr_dB_out = []
    # loop over frames
    while True:
        skc_log.info(f'frame number: {frame}')         

        # FRAME SYNCHRONIZATION
        est_shift = np.zeros(sig_rx.n_dims, dtype=np.int64)        
        # first frame is handled differently: search range for frame sync is longer (whole frame) and "DGD detection" is performed
        if frame == 0:
            # frame sync (loop over all rx dims)
            for dim in np.arange(sig_rx.n_dims):                
                est_shift[dim] = da_frame_sync(samples = sig_rx.samples[dim],
                                               shift = int(sig_tx.info[dim]['fs_header']['length']/4*oversampling),
                                               search_range = frame_len*oversampling,
                                               dbg_plot = (dim==dbg_dim) and (frame==dbg_frame) and dbg_plt,
                                               fNum = 255,
                                               exec_mode='auto')

            # Assuming two polarizations with timedelay and low DGD, a worst case szenario could be an estimated shift of [65536, 0] for X and Y.
            # In this case, both polarizations will shifted to a non-matching phase-noise-random walk.
            # Therefore we try to estimate, if the difference in shift between both polarizations is larger than X, please set both shift difference to one value.
            if np.max(est_shift)-np.min(est_shift) > 0.9*frame_len:
                est_shift = np.array([np.max(est_shift), np.max(est_shift)])

        else: # for remaining frames, the frame sync search range can be very short: only to compensate possible sampling clock offsets
            search_range = int(200) # search range in samples around previous frame end           
            # frame sync (loop over all rx dims)
            for dim in np.arange(sig_rx.n_dims):                
                est_shift[dim] = da_frame_sync(samples = sig_rx.samples[dim][stop_sample[dim]-frame_overlap-int(search_range/2):], 
                                               shift = int(sig_tx.info[dim]['fs_header']['length']/4*oversampling), 
                                               search_range = search_range, 
                                               dbg_plot = (dim==dbg_dim) and (frame==dbg_frame) and dbg_plt, 
                                               fNum = 255, 
                                               exec_mode = 'auto')
            est_shift = est_shift - int(search_range/2)
        
        skc_log.info(f'Frame {frame}: est. shift: {est_shift} samples')

        start_sample = start_sample + est_shift + bool(frame)*frame_len*oversampling
        stop_sample  = start_sample + frame_len*oversampling + frame_overlap
        
        # last frame
        if np.max(stop_sample) > sig_rx.samples[0].size:
            # remainder less than one frame (without frmae_overlap)?
            if np.max(stop_sample-frame_overlap+frame_phase_average) > sig_rx.samples[0].size:
                break
            # reduce frame_overlap
            else:
                frame_overlap = np.min(sig_rx.samples[0].size - (stop_sample-frame_overlap))
                stop_sample = start_sample + frame_len*oversampling + frame_overlap
        
        # build array for samples of current frame        
        # shape (sig_rx.n_dims, frame_len*oversampling+frame_overlap)
        samples = np.zeros((sig_rx.n_dims, frame_len*oversampling + frame_overlap), dtype=np.complex128) * np.nan
        for dim in np.arange(sig_rx.n_dims):            
            samples[dim, :] = sig_rx.samples[dim][start_sample[dim]:stop_sample[dim]] 

        # CARRIER FREQUENCY-OFFSET COMPENSATION
        if cfo_comp:
            # coarse CFO compensation based only on sub-frame-sync header sequence (short->large "catch range", but not accurate)
            # NOTE: Please respect catch-ranges (CFO with additional phase noise) of the equalizer! otherwise est cfo will toggle (+/-) and therefore mess up the estimation!
            # define CFO catch range
            cfo_coarse_catch_range = 0.5*sig_rx.sample_rate[dim]/(sig_rx.info[dim]['fs_header']['n_symb']*oversampling)
            coarse_cfo = np.zeros(sig_rx.n_dims, dtype=np.float64)              
            for dim in np.arange(sig_rx.n_dims):                
                # perform coarse CFO multiple times with shift len(A) over different versions of [A,A,-A,A] and average
                sub_frame_len = (sig_tx.info[dim]['fs_header']['n_symb']*4*oversampling)
                n_reps = int(sig_tx.info[dim]['fs_header']['length']*oversampling/sub_frame_len)
                tmp_cfo = np.zeros(n_reps, dtype=np.float64)
                for rep in np.arange(n_reps):
                    tmp_cfo[rep] = da_cfo_estimation(samples=samples[dim,rep*sub_frame_len:], shift=int(sig_rx.info[dim]['fs_header']['n_symb']*oversampling),
                                                    sample_rate=sig_rx.sample_rate[dim])
                if np.abs(np.diff([np.min(tmp_cfo), np.max(tmp_cfo)])) > cfo_coarse_catch_range:
                    warnings.warn("CFO estimation could be (+/-) swapped! Did you notice the CFO coarse catch range?")
                coarse_cfo[dim] = np.mean(tmp_cfo)
            # compensate mean coarse CFO (over all dimensions)      
            coarse_cfo_mean = np.mean(coarse_cfo)            
            t = np.arange(samples[dim].size)/sig_rx.sample_rate[dim]
            samples = samples * np.exp(-1j * 2 * np.pi * coarse_cfo_mean * t)

            # fine CFO
            fine_cfo = np.zeros(sig_rx.n_dims, dtype=np.float64)
            for dim in np.arange(sig_rx.n_dims):
                fine_cfo[dim] = da_cfo_estimation(samples=samples[dim,:], shift=int(fs_header_len/4*oversampling),
                                                  sample_rate=sig_rx.sample_rate[dim])
            # compensate mean fine CFO (over all dimensions)            
            fine_cfo_mean = np.mean(fine_cfo)
            samples = samples * np.exp(-1j * 2 * np.pi * fine_cfo_mean * t)    

            CFO_out.append(coarse_cfo_mean+fine_cfo_mean)
            
            skc_log.info(f'Frame {frame}: est. CFO (coarse): {coarse_cfo/1e6} MHz, mean coarse CFO: {coarse_cfo_mean/1e6:.3f} MHz')
            skc_log.info(f'Frame {frame}: est. CFO (fine): {fine_cfo/1e6} MHz, mean fine CFO: {fine_cfo_mean/1e6:.3f} MHz')
            skc_log.info(f'Frame {frame}: est. CFO (coarse+fine): {(coarse_cfo+fine_cfo)/1e6} MHz, mean fine+coarse CFO: {(coarse_cfo_mean+fine_cfo_mean)/1e6:.3f} MHz')

        # CHANNEL ESTIMATION AND EQUALIZATION
        ce_header_rx = samples[:,fs_header_len*oversampling:(fs_header_len+ce_header_len)*oversampling] # extract channel estimation headers (all dimensions)
        
        # remove first and last OFDM blocks of CE header (these are used as guard intervals to avoid ISI)
        ce_header_rx = ce_header_rx[:,sig_tx.info[0]['ce_header']['n_fft']*oversampling:-sig_tx.info[0]['ce_header']['n_fft']*oversampling]
        n_repetitions = sig_tx.info[0]['ce_header']['n_repetitions'] - 2

        # SNR estimation ("discrete" spectrum of CE header due to repetition (`n_repetitions`) which can be used to estimate noise between pilots)
        # in order to calc "SNR per Rx dimension", power of all Tx-dimension pilots has to be assessed to store estimated signal and noise powers for each "Rx path"
        p_n = np.zeros(sig_rx.n_dims, dtype=np.float64) * np.nan   # array of noise powers (per dimension)
        p_sig = np.zeros(sig_rx.n_dims, dtype=np.float64) * np.nan # array of signal powers (per dimension)
        pilot_pos = np.sort(np.unique(np.hstack([sig_tx.info[dim]['ce_header']['pilot_pos'] for dim in np.arange(sig_tx.n_dims)])))  # array of pilot indices from all transmit dimensions
        
        for rx_dim in np.arange(sig_rx.n_dims):
            snr_est_results = da_snr_estimation(ce_header_rx=ce_header_rx[rx_dim,:], pilot_pos=pilot_pos, n_fft=sig_rx.info[rx_dim]['ce_header']['n_fft'],
                                                n_repeat=n_repetitions, pilot_neighbors=3, windowed=False, bw_limit=0.25*(1+1), dbg_plot=False, fNum=None)

            p_n[rx_dim]   = snr_est_results['p_n']
            p_sig[rx_dim] = snr_est_results['p_sig']

        snr_dB_out.append(10*np.log10(p_sig/p_n))
        skc_log.info(f'Frame {frame}: estimated SNR (per Rx aperture): {10*np.log10(p_sig/p_n)} dB')         
        
        # average all remaining CE headers and go into frequency domain        
        ce_header_rx_avg = ce_header_rx.reshape(ce_header_rx.shape[0], n_repetitions, -1)
        ce_header_rx_avg = ce_header_rx_avg.mean(axis=1)     # averaging over n_repetitions
        ce_header_rx_avg_spec = np.fft.fft(ce_header_rx_avg)
        # because IFFT has scaling factor 1/N -> when using oversampling at Rx the pilots will be factor 2 larger which has to be compensated for
        # TODO: CHECK reasoning: Maybe simply different n_fft @tx and @rx
        ce_header_rx_avg_spec /= oversampling

        if dbg_plt and (frame==dbg_frame):
            plt.figure(305).clear()
            fig, axs = plt.subplots(nrows=ce_header_rx_avg_spec.shape[0], ncols=1, squeeze=False, num=305)            
            for dim in np.arange(ce_header_rx_avg_spec.shape[0]):
                axs[dim,0].clear()
                axs[dim,0].plot(np.fft.fftshift(np.fft.fftfreq(ce_header_rx_avg_spec[dim].shape[0],d=1/sig_rx.sample_rate[dim])),20*np.log10(np.abs(np.fft.fftshift(ce_header_rx_avg_spec[dim]))))
            fig.suptitle(f'frame {frame}: abs of averaged received pilots')

        # channel estimation        
        # to store estimated MIMO frequency responses 
        # H_est.shape = (n_rx, n_tx, n_pilots*2)
        H_est = np.zeros((sig_rx.n_dims, sig_tx.n_dims, sig_tx.info[0]['ce_header']['n_pilots']*oversampling), dtype=np.complex128) * np.nan        
        for rx_dim in np.arange(sig_rx.n_dims):
            for tx_dim in np.arange(sig_tx.n_dims):                    
                H_est[rx_dim, tx_dim,:] = da_channel_estimation(ce_header_rx_spec=ce_header_rx_avg_spec[rx_dim,:], 
                                                                pilots=sig_tx.info[tx_dim]['ce_header']['pilots'],
                                                                pilot_pos=sig_tx.info[tx_dim]['ce_header']['pilot_pos'], 
                                                                n_fft=sig_tx.info[tx_dim]['ce_header']['n_fft'])
                
        H_out.append(H_est)            
        
        if dbg_plt and (frame==dbg_frame):
            plt.figure(311).clear()
            fig, axs = plt.subplots(nrows=sig_rx.n_dims, ncols=sig_tx.n_dims, squeeze=False, num=311, sharex='all', sharey='all')            
            f = np.fft.fftfreq(H_est.shape[2], 1/sig_rx.sample_rate[0])
            # plot estimated frequency responses
            for rx_dim in np.arange(sig_rx.n_dims):
                for tx_dim in np.arange(sig_tx.n_dims):
                    axs[rx_dim, tx_dim].clear()
                    axs[rx_dim, tx_dim].plot(np.fft.fftshift(f),20*np.log10(np.fft.fftshift(np.abs(H_est[rx_dim,tx_dim]))))
                    axs[rx_dim, tx_dim].plot(np.fft.fftshift(f),np.unwrap(np.angle(np.fft.fftshift(H_est[rx_dim,tx_dim]))))                        
                    axs[rx_dim, tx_dim].grid(visible=True, which='both')
                    axs[rx_dim, tx_dim].set_title(f'{tx_dim}=>{rx_dim}')
            fig.suptitle(f'frame {frame}: Estimated frequency response(s): Tx_dim => Rx_dim')
            fig.tight_layout()          

            # plot estimated impulse responses % TODO: fft-shit time-axis and h(t) to plot deterministic pulse response (-T/2 ... +T/2)
            h_est = np.fft.ifft(H_est, axis=-1)
            plt.figure(312).clear()
            fig, axs = plt.subplots(nrows=sig_rx.n_dims, ncols=sig_tx.n_dims, squeeze=False, num=312, sharex='all', sharey='all')            
            t = np.arange(h_est.shape[-1])/sig_rx.sample_rate[0]
            for rx_dim in np.arange(sig_rx.n_dims):
                for tx_dim in np.arange(sig_tx.n_dims):
                    axs[rx_dim, tx_dim].clear()
                    axs[rx_dim, tx_dim].plot(t, np.abs(h_est[rx_dim,tx_dim]))
                    axs[rx_dim, tx_dim].grid(visible=True, which='both')
                    axs[rx_dim, tx_dim].set_title(f'{tx_dim}=>{rx_dim}') 
            fig.suptitle(f'frame {frame}: Estimated impulse response(s) (abs): Tx_dim => Rx_dim')
            fig.tight_layout()          

        
        # calc EQ frequency response            
        # to store EQ frequency responses
        # W.shape = (n_tx, n_rx, n_fft*2)
        W = calc_eq_frequency_response_zf(H_est)
        W_out.append(W)
        # calc impulse response
        # w.shape = (n_tx, n_rx, n_fft*2)
        w = np.fft.ifft(W, axis=-1)

        if dbg_plt  and (frame==dbg_frame):
            plt.figure(315).clear()
            fig, axs = plt.subplots(nrows=sig_rx.n_dims, ncols=sig_tx.n_dims, squeeze=False, num=315, sharex='all', sharey='all')            
            f = np.fft.fftfreq(W.shape[-1], 1/sig_rx.sample_rate[0])
            # plot equalizer frequency responses (watch out: arrangement like for H and h ([rx_dim, tx_dim]))
            for rx_dim in np.arange(sig_rx.n_dims):
                for tx_dim in np.arange(sig_tx.n_dims):
                    axs[rx_dim, tx_dim].clear()
                    axs[rx_dim, tx_dim].plot(np.fft.fftshift(f),20*np.log10(np.fft.fftshift(np.abs(W[rx_dim,tx_dim]))))
                    axs[rx_dim, tx_dim].plot(np.fft.fftshift(f),np.unwrap(np.angle(np.fft.fftshift(W[rx_dim,tx_dim]))))
                    axs[rx_dim, tx_dim].grid(visible=True, which='both')
                    axs[rx_dim, tx_dim].set_title(f'{tx_dim}=>{rx_dim}') 
            fig.suptitle(f'frame {frame}: EQ frequency response(s): Rx_dim => Tx_dim')  
            fig.tight_layout()                    

            # plot abs of equalizer impulse responses
            plt.figure(316).clear()
            fig, axs = plt.subplots(nrows=sig_rx.n_dims, ncols=sig_tx.n_dims, squeeze=False, num=316, sharex='all', sharey='all')            
            t = np.arange(w.shape[-1])/sig_rx.sample_rate[0]            
            for rx_dim in np.arange(sig_rx.n_dims):
                for tx_dim in np.arange(sig_tx.n_dims):
                    axs[rx_dim, tx_dim].clear()
                    axs[rx_dim, tx_dim].plot(t, np.abs(w[rx_dim,tx_dim]))
                    axs[rx_dim, tx_dim].grid(visible=True, which='both')
                    axs[rx_dim, tx_dim].set_title(f'{tx_dim}=>{rx_dim}') 
            fig.suptitle(f'frame {frame}: EQ impulse response(s) (abs): Rx_dim => Tx_dim')  
            fig.tight_layout()             

            modMax = np.max(np.concatenate((np.abs(sig_rx.constellation[-1].real), np.abs(sig_rx.constellation[-1].imag))))
            visualizer.plot_constellation(samples[dbg_dim,:][(fs_header_len+ce_header_len)*oversampling:-frame_overlap:oversampling], 
                                        fNum=400,hist=True,axMax=modMax*1.5,
                                        tit=f'frame {frame}: received, sampled, constellation (dimension {dbg_dim})')
            
            for dim in np.arange(sig_rx.n_dims):
                plt.figure(555+dim)
                plt.figure(555+dim).clear()
                f = np.fft.fftfreq(samples.shape[1], 1/sig_rx.sample_rate[dim])
                plt.plot(np.fft.fftshift(f),np.fft.fftshift(np.abs(np.fft.fft(samples[dim,:]))), label='input signal')
        
        # equalization
        # loop over impulse responses    
        # make filter causal (shift center of impulse response to middle)
        samples_eq = np.zeros((w.shape[0], samples.shape[1]+w.shape[2]-1), dtype=np.complex128)
        for tx_dim in np.arange(sig_tx.n_dims):
            for rx_dim in np.arange(sig_rx.n_dims):                
                w_causal = np.fft.fftshift(w[tx_dim, rx_dim])
                samples_eq[tx_dim,:] += ssignal.oaconvolve(samples[rx_dim,:], w_causal, mode='full')
        # undo shift (cut away first samples, which are in the header anyway)
        # TODO: * check if this produces errors for later phase jump estimation!!!!!! --> this probably has to be changed, LUTZ!
        #       * check if filter ringing at the end of the signal needs to be cut away
        eq_delay = int(w_causal.size/2)
        samples = samples_eq[:, eq_delay:eq_delay+frame_len*oversampling+frame_overlap]

        # TODO: SNR estimation could be done here on separated polarizations if necessary !!!

        if dbg_plt  and (frame==dbg_frame):
            for dim in np.arange(sig_rx.n_dims):
                plt.figure(555+dim)
                f = np.fft.fftfreq(samples.shape[1],1/sig_rx.sample_rate[dim])
                plt.plot(np.fft.fftshift(f),np.fft.fftshift(np.abs(np.fft.fft(samples[dim,:]))),alpha=0.5, label='output signal')    
                plt.legend()
                plt.title(f'frame {frame}: spectra before and after EQ ({dim} dimension)')

        # find phase jumps between adjacent frames due to later header removal
        for tx_dim in np.arange(sig_tx.n_dims):
            # find phase difference of adjacent frames (end of last frame to beginning of data in current frame) 
            # -> has to be compensated for in order to avoid cycle slips
            # -> consists of two components:
            #       1) phase difference within header (has to be compensated for, since header is removed) -> header_phase_diff
            #       2) phase difference between adjacent frames originating from different CFO and equalization -> frame_phase_diff
            
            # 1) estimate phase difference between header start and end due to CFO compensation and equalization
            header_orig = np.concatenate((sig_tx.info[tx_dim]['fs_header']['header'], sig_tx.info[tx_dim]['ce_header']['header']))
            # two methods possible:
            # either A) ...
            # compare sent header to received header to get phase signature of header
            header_phase_signature = np.angle(samples[tx_dim, 0:(ce_header_len+fs_header_len)*oversampling:oversampling] * np.conj(header_orig))
            # linear fit TODO: check if quadratic, cubic or higher order fit is more robust than linear
            p = np.polynomial.Polynomial.fit(np.arange(header_phase_signature.size), header_phase_signature, 1)            
            # find header phase difference (betweeen end and start) from linear fit
            header_phase_diff = p(header_phase_signature.size)-p(0)

            # # ... or B)
            # N = 60
            # find angle of correlation coefficient for the first / last N header symbols
            # header_start_phase = np.angle(np.dot(samples[tx_dim, 0:N*oversampling:oversampling],np.conj(header_orig[0:N])))
            # header_end_phase = np.angle(np.dot(samples[tx_dim, (ce_header_len+fs_header_len-N)*oversampling:(ce_header_len+fs_header_len)*oversampling:oversampling],np.conj(header_orig[-N:])))
            # header_phase_diff = header_end_phase-header_start_phase
            # # --> TODO: check which of the methods is more robust!!!

            # 2) estimate phase difference between previous and current block:
            #  compare first header samples from the same header, which have seen processing from previous frame
            if frame == 0:
                # save the first samples of the next frame, but with processing from this frame for phase difference compensation
                first_samples_prev_frame[tx_dim,:] = samples[tx_dim,frame_len*oversampling:frame_len*oversampling+frame_phase_average]         
                frame_phase_diff = 0.0                   
            else:                
                # calc phase difference due to different frame processing (and CFO)
                frame_phase_diff = np.angle(np.dot(np.conj(first_samples_prev_frame[tx_dim,:]),samples[tx_dim,0:frame_phase_average]))                
                # save the first samples of the next frame with current processing for the next phase difference calculation/compensation
                first_samples_prev_frame[tx_dim,:] = samples[tx_dim,frame_len*oversampling:frame_len*oversampling+frame_phase_average]            
            
            # (accumulated) phase difference between frames after header removal
            phase_jumps[tx_dim] = phase_jumps[tx_dim] + frame_phase_diff + header_phase_diff            
        
        # cut header and frame overlap
        samples = samples[:, header_len*oversampling:-frame_overlap:]

        # downsampling
        if decimate:
            samples = samples[:,::oversampling]            
        
        if dbg_plt  and (frame==dbg_frame):
            modMax = np.max(np.concatenate((np.abs(sig_rx.constellation[-1].real), np.abs(sig_rx.constellation[-1].imag))))
            visualizer.plot_constellation(samples[dbg_dim,:],fNum=410,hist=True,axMax=modMax*1.5,tit=f'frame {frame}: received equalized, (sampled) constellation (dimension {dbg_dim})')

        # eliminate phase jumps between frames due to 
        #   * frame independent CFO compensation and equalization 
        #   * header removal
        samples = samples * np.exp(-1j * phase_jumps[:,np.newaxis])

        samples_out = np.concatenate((samples_out, samples),axis=1)

        frame += 1      

    if dbg_plt:
        plt.figure(420)
        plt.figure(420).clear()
        plt.plot(np.unwrap(np.angle(samples_out[dbg_dim,:])))
        plt.title('phase of equalized samples after header removal and decimation')
        plt.ylabel('unwraped phase / rad')
        plt.xlabel('sample #')
        plt.show()

    # construct sig_rx.samples from samples
    sig_rx.samples = samples_out
    if decimate:
        sig_rx.sample_rate = sig_rx.symbol_rate[0]

    # generate results dict
    results = dict()
    results['sig_rx'] = sig_rx
    results['W_out'] = W_out
    results['H_out'] = H_out
    results['CFO_out'] = CFO_out
    results["snr_dB_out"] = snr_dB_out

    return results

def coRx_phase_imbalance_comp(samples, switch_IQ=False, two_theta=None, orthogonalization_method="GSOP"):
    """
    Phase imbalance compensation for a coherent receiver frontend. 

    Parameters
    ----------
    samples: 1D numpy array, real or complex
        input samples
    switch_IQ : bool
        flag if I/Q should be switched for GSOP calculation / application of phase and amplitude if specified.
        Default is False. 
    two_theta : float
        Phase deviation from 90° of the optical hybrid. Angle between I/Q-components is defined as (pi/2 - two_theta) in rad. 
        If two_theta is None, the correction angle is estimated with corr-coef variant. If value is given, this value is used for compensation.
        Default is None.
    orthogonalization_method : string
        Orthogonalizsation method used for correction phase deviation of the optical hybrid. Could be "GSOP" for Gram-Schmidt orthogonalization or
        "Loewdin" for Loewdin-orthogonalization. If a not matching string is passed, no phase deviation is compensated.
        Default is GSOP.

    Returns
    -------
    results:  dict containing following keys
        samples: 1D numpy array, real or complex
            samples with corrected imbalance
        two_theta: float 
            Phase deviation from 90° of the optical hybrid in rad.
        ampl_imbalance_dB: float
            amplitude imbalance between I and Q components, as defined by 20*np.log10(std(I)/std(Q)) [dB].

    References
    ----------   
    [1] I. Fatadin, S. J. Savory, and D. Ives, “Compensation of Quadrature Imbalance in an Optical QPSK Coherent Receiver,” IEEE Photonics Technology Letters, vol. 20, no. 20, pp. 1733-1735, Oct. 2008, doi: 10.1109/lpt.2008.2004630.
    
    [2] S. J. Savory, “Digital Coherent Optical Receivers: Algorithms and Subsystems,” Selected Topics in Quantum Electronics, IEEE Journal of, vol. 16, no. 5, pp. 1164-1179, Sep. 2010.
    """
    # get I, Q
    r_i = np.real(samples)
    r_q = np.imag(samples)

    # switch I, Q if option is given 
    if switch_IQ is True:
        r_i, r_q = r_q, r_i
    
    # I-Q DC-block and normalization to unit variances
    m_i, m_q = r_i.mean(), r_q.mean()
    s_i, s_q = r_i.std(), r_q.std()
    r_i, r_q = r_i-m_i, r_q-m_q

    # if two_theta is given, use this values and don't perform a GSOP
    if two_theta is not None:
        sin_2theta = np.sin(two_theta)
    else: 
        # if not, estimate hybrid phase deviation (correlation / projection method)
        sin_2theta  = np.corrcoef(r_i,r_q)[1][0] # eq.(6) in [2]
        two_theta = np.arcsin(sin_2theta)
    
    # doing orthogonalizatio as configured
    if orthogonalization_method == "GSOP": 
        # Gram–Schmidt Orthogonalization (GSOP)
        M = np.array([[1, 0], [-sin_2theta/np.cos(two_theta), 1/np.cos(two_theta)]]) # eq.(7) in [2] + Normalization of r2
        # NOTE: In difference to [2], we divide through cos(2theta). We have to check (TODO) why this is the right performance 
        # in difference to multiply by cos(2theta)
    elif orthogonalization_method == "Loewdin":
        # Loewdin Orthogonalization
        cos, cos2, tan2 = np.cos(two_theta/2), np.cos(two_theta), np.tan(two_theta)
        M = np.array([[cos/cos2, -tan2/(2*cos)], [-tan2/(2*cos),cos/cos2]]) # eq.(8) in [2]
    else:
        # unit matrix - no orthogonalization performed
        M = np.array(([1,0],[0,1]))
     
    Mr = M @ np.vstack((r_i/s_i,r_q/s_q))
    I_0, Q_0 = Mr[0]*s_i + m_i, Mr[1]*s_q + m_q

    # switching back I, Q if option is selected
    if switch_IQ is True:
        I_0, Q_0 = Q_0, I_0

    # add return values
    return_dict = {"samples": I_0 + 1j*Q_0, "two_theta": two_theta, "ampl_imbalance_dB": 20*np.log10(s_i/s_q)}
    return return_dict

def dc_block_and_unit_variance_norm(samples):
    """
    Function to cover DC block and normalization to unit variance. Operation is performed on I and Q individual.
    
    Implementation:
    :math:`r' = \\frac{r - \mu}{\sqrt{2} \cdot \sigma}`

    where :math:`\mu` is the mean and :math:`\sigma` is the standard deviation
    of the respective component.

    Parameters
    ----------
    samples: 1D numpy array, complex
        input signal

    Returns
    -------
    samples: 1D numpy array, complex
        processed signal

    """

    # get I, Q
    r_i = np.real(samples)
    r_q = np.imag(samples)

    # I-Q DC-block and normalization to unit variances
    r_i, r_q = (r_i-r_i.mean())/(np.sqrt(2)*r_i.std()), (r_q-r_q.mean())/(np.sqrt(2)*r_q.std())

    return r_i+1j*r_q

def pilot_aided_coarse_cpe(samples, pilot_symbols, M=100, N=1, debug=True):
    """
    Pilot-aided coarse carrier-phase-estimation (and correction). Used as a first stage of a two stage CPE procedure.
    With the help of the pilot sequence, absolute phase difference is calculated. With the help of a triangle-shaped filter 
    (which is suboptimal, but not wrong in total as seen in reference), a interpolation to symbol rate is performed.
    Therefore phase offset is corrected. 
    NOTE: Please be aware of boundary effects of filter interpolation in corrected samples. Use filter len to crop if needed.

    Parameters
    ----------
    samples : 1D numpy array, complex
        input signal on symbol frequency (= 1sps)   
    pilot_symbols : 1D numpy array, complex
        zeropadded pilot symbols (= 1sps). Should have the appereance of [pilot1, 0+0j, 0+0j, ..., pilot2, ...] 
    M : int
        pilot spacing in samples between to pilots. 
        Default is 100 (corresponds to 1% pilot rate).
    N : int
        avaraging factor N, which makes the filter more width to handle lower SNR.
        Default is 1.
    debug : bool
        Debug flag. If true, some plots are generated.
        Default is True.

    Returns
    -------
    cpe_results : dict containing following keys
        samples_corrected : 1D numpy array, real or complex
            input symbols with recovered coarse carrier phase.
        est_phase_noise_coarse : 1D numpy array, real
            estimated coarse random phase walk.
        fitler_len : int
            filter width. In this range (from start and from end) of
            corrected samples, boundary effects form interpolation may be visible.
            Use filter width for cropping if needed.
                    
    References
    ----------
    [1] M. Magarini et al., “Pilot-Symbols-Aided Carrier-Phase Recovery for 100-G PM-QPSK Digital Coherent Receivers,” IEEE Photonics Technology Letters, vol. 24, Art. no. 9, May 2012, doi: 10.1109/lpt.2012.2187439.
    """
    # some error checking
    if len(pilot_symbols) != len(samples):
        raise Exception("length of samples and (zeropadded) pilot symbols should be equal!")

    ### first stage: pilot symbols phase estimator ## 
    # multiply conj samples with pilots and doing zeropad
    pilot_pos = np.nonzero(pilot_symbols)[0] 
    d_k = np.zeros(pilot_symbols.shape, dtype=complex)
    d_k[pilot_pos] = samples[pilot_pos]*np.conj(pilot_symbols[pilot_pos])
    #d_k[::M] = np.unwrap(np.angle(samples[::M]*np.conj(symbols[::M])))

    ## interpolate with filter
    # generate impulse response (triangle shaped) of suboptimal filter h_k
    k = np.arange(-(N*M)-10,(N*M)+11)
    h_k = np.zeros(k.shape)
    for i in range(len(k)):
        if k[i] < N*M and k[i] > -(N*M):
            h_k[i] = (1/(M*N**2)) * (M*N-np.abs(k[i]))
        else:
            h_k[i] = 0

    if h_k.size > samples.size:
        raise Exception("to large filter design for pilot based CPE. Correct settings!")

    # convolve impulse response and phase pertubation
    z_i = np.convolve(h_k, d_k, mode="same")

    if debug is True:
        plt.figure()
        plt.title("theory from spalvieri 2009 \n impulse response")
        plt.plot(k,h_k, label="suboptimal h(k)")
        plt.plot([-M, -M, M, M], [min(h_k), max(h_k), max(h_k), min(h_k)], 
                label="M (pilot rate)", linestyle="--", color="k")
        plt.plot([-N*M, -N*M, N*M, N*M], [min(h_k), max(h_k), max(h_k), min(h_k)], 
                label="N (SNR weigth factor)", linestyle="--", color="r")
        plt.grid(which="both")
        plt.legend(loc="lower left")

        plt.figure()
        plt.title("theory from spalvieri 2009 \n zeropad + interpol filter")
        #plt.plot(phaseAcc[crop:-crop][0::int(sps)]*(180/np.pi), label="PhaseAcc")
        plt.plot(np.unwrap(np.angle(d_k))*(180/np.pi), label="zeropad diff phase est", marker="o", linestyle="")
        plt.plot(np.unwrap(np.angle(z_i))*(180/np.pi), label="interpol coarse phase est.", linestyle="--", color="green")
        plt.xlabel("Samples")
        plt.grid(which="both")
        plt.legend(loc="lower left")

    # apply phase difference on samples for coarse phase noise estimation
    est_coarse_phase = np.unwrap(np.angle(z_i))
    samples = samples*np.exp(-1j*est_coarse_phase)

    # just return values which are there if we dont to bps CPE
    cpe_results = dict()
    cpe_results['samples_corrected'] = samples
    cpe_results['est_phase_noise_coarse'] = est_coarse_phase
    cpe_results['coarse_cpe_filter_len'] = len(h_k)

    return cpe_results

def cycle_slip_correction(coarse_phase_est, fine_phase_est, quantize_value=np.pi/2, jump_point_avag=1, debug=False):
    """
    Function to compensate for cycle slips based on corase and fine phase estimation. Here,
    the assumption is that coarse phase est cant have cycle slips. Therefore, the fine phase estimation 
    is limited to +-quantize_value/2. If values bigger than these treshold are visible, these values will be assumed
    as cycle slip and therefore be corrected.

    Parameters
    ----------
    coarse_phase_est : 1D numpy array, real
        coarse phase estimation array
    fine_phase_est : 1D numpy array, real
        fine phase estimation array
    quantiue_value : float
        value for detecting cycle slip. +-quantize_value/2 is cycle slip treshold.
        Default is pi/2.
    jump_point_avag : int
        avaraging of cycle slip offset array. Used to pretend from hard jumps in array.
        Default is 1.
    debug : bool
        debug flag. If True, plots are generated.
        Default: False.

    Returns
    -------
    fine_phase_est_corrected : 1D numpy array, real
        corrected fine phase estimation.
                    
    References
    ----------
    None
    """
    # this step is (1-0)-1 -> so this is not the efficient way to do it. we dont need coarse phase est here.
    phase_est_overall = coarse_phase_est[:len(fine_phase_est)] + fine_phase_est 
    difference = coarse_phase_est[:len(fine_phase_est)] - phase_est_overall
    
    # we want to quantize to np.pi/2, np.pi ...
    cycle_slip_offset = quantize_value * np.round(difference/(quantize_value))

    if jump_point_avag is not None:
        # correct for jump points with moving avarage
        cycle_slip_offset_ma = uniform_filter1d(cycle_slip_offset, size=jump_point_avag)
    else:
        cycle_slip_offset_ma = cycle_slip_offset
    
    # correct the fine_phase our cycle slip findings
    fine_phase_est_corrected = fine_phase_est+cycle_slip_offset_ma

    if debug is True:
        plt.figure(101)
        plt.plot(fine_phase_est)
        plt.plot(fine_phase_est_corrected)

    return fine_phase_est_corrected

def pilot_based_cpe_wrapper(sig_rx, pilot_average=5, tap_len=31, test_phases=31, remove_filter_effects=True, debug=True, fNum=None):
    """
    Wrapper function to process three-staged CPE. The following process will be done:
    1. a pilot-based CPE as coarse first stage, based on pilot symbols. With an interpolation filter, based on the difference between 
    noisy received samples and know pilot symbols, a coarse phase offset will be estimated and interpolated on all samples.
    2. BPS as fine second stage. Since pilot-based CPE is to slow to catch all phase noise effects, we use the BPS as fast second stage fine estimation
    3. cycle slip correction as last stage. Due to the possibility of cycle slips in BPS, we correct for cycle slips by assuming pilot-based CPE is
    immune to cylce slips. If BPS produces phase jumps higher than e.g. pi/2, we assume a cycle slip here and correct for by put offset of e.g. -pi/2. 

    NOTE: The function assumes that pilots in sig_rx object and samples in sig_rx object are in sync and samples with 1 symbol per sample. 
    
    NOTE: Function can be applied to single or multiple frame receiver. Function will calculate amount of frames by checkign samples vs symbols.

    Parameters
    ----------
    sig_rx : signal-class object
        n-dimensional signal object with list of sample arrays in the 'samples'
        attribute.
    pilot_average : int
        average parameter to average for Noise in interpolation filter of coarse cpe. Please see related function for further details. Default is 5.
    tap_len : int
        number of taps for BPS. Please NOTE: in case of frame-structured receiver, tap_len is used to choose a close tap_len which is a full divisior of the payload. Default is 31.
    test_phases : int
        number of testphase for BPS. Default is 31.
    remove_filter_effects : bool
        If True, filter effects from coarse CPE will we cropped on both sides of the sample array. This leads to crop frame-whise in the front if frame-structured receiver is applied, 
        to keep synchronisation. Default is True.
    debug : bool
        debug flag, using plots. Default is True.
    fNum : int
        number of figure to be generated if Debug is True. Default is None.

    Returns
    -------
    sig_rx : signal-class object
        processed signal object, samples with estimated and compensated carrier phase noise
    cpe_results_dict : dict containing following keys
        dim : int, nested dict containing following keys per dimension
            est_phase_noise_coarse : array
                course random phase walk estimated by the first stage (pilot-based) in rad
            est_phase_noise_fine_before_cs_correction : array
                fine random phase walk estimated by the second stage (BPS) in rad
            est_phase_noise_fine_after_cs_correction : array
                fine random phase walk estimated by the second stage (BPS) in rad, with cycle slips corrected
    """
    # add nested dict for results
    cpe_results_dict = {}
    for dim in range(sig_rx.n_dims):
        cpe_results_dict[dim] = {}

    # do phase estimation and compensation on each dim
    for dim in range(sig_rx.n_dims):
        # check if pilot symbols are avaiable - otherwise error!
        if 'cpe_pilot_info' not in sig_rx.info[dim].keys():
            raise Exception("No CPE pilots in signal in dim {}! can't perform pilot based CPE".format(dim))
        # get pilot rate based on difference ob pilot-positions
        pilot_rate = sig_rx.info[dim]['cpe_pilot_info']['pilot_pos'][1]-sig_rx.info[dim]['cpe_pilot_info']['pilot_pos'][0]
        
        # prepare coarse phase estimation - generate matching pilot array
        pilot_array = np.zeros_like(sig_rx.samples[dim])
        payload_len = sig_rx.symbols[dim].size + sig_rx.info[dim]['cpe_pilot_info']['cpe_pilots'].size
        n_dsp_frames = sig_rx.samples[dim].size/payload_len
        for frame in np.arange(n_dsp_frames):
            pilot_array[sig_rx.info[dim]['cpe_pilot_info']['pilot_pos']+int(frame*payload_len)] = sig_rx.info[dim]['cpe_pilot_info']['cpe_pilots']
            
        # doing coarse phase estimation
        cpe_coarse_results = pilot_aided_coarse_cpe(sig_rx.samples[dim], pilot_array, M=pilot_rate, N=pilot_average, debug=False)
        sig_rx.samples[dim] = cpe_coarse_results['samples_corrected']
        
        # if selected, crop samples according to filter length of coarse phase estimation to avoid filter effects
        if remove_filter_effects is True:
            start_offset = 0
            if n_dsp_frames != 1:
                # roll back all these arrays (if multiple, concatenating frames is assumed) to set same start as before cropping
                # crop whole first(s) frames depending on filter len
                start_offset = int( np.ceil(cpe_coarse_results['coarse_cpe_filter_len'] / payload_len)*payload_len ) - cpe_coarse_results['coarse_cpe_filter_len']

            sig_rx.samples[dim] = sig_rx.samples[dim][cpe_coarse_results['coarse_cpe_filter_len']+start_offset:-cpe_coarse_results['coarse_cpe_filter_len']]
            pilot_array = pilot_array[cpe_coarse_results['coarse_cpe_filter_len']+start_offset:-cpe_coarse_results['coarse_cpe_filter_len']]
            cpe_coarse_results['est_phase_noise_coarse'] = cpe_coarse_results['est_phase_noise_coarse'][cpe_coarse_results['coarse_cpe_filter_len']+start_offset:-cpe_coarse_results['coarse_cpe_filter_len']]

        # blind fine phase processing from this point
        if payload_len%tap_len:
            payload_len_arr = np.arange(1, payload_len+1, step=1) # build array up from 1 to payload len and % target tap length
            payload_len_arr = payload_len_arr[(payload_len)%payload_len_arr == 0]
            tap_len = payload_len_arr[(np.abs(payload_len_arr - tap_len)).argmin()] # find closest value to specified tap_len

        # doing fine phase estimation
        cpe_fine_results = carrier_phase_estimation_bps(sig_rx.samples[dim], sig_rx.constellation[dim], 
                                                            n_taps=tap_len, n_test_phases=test_phases, 
                                                            const_symmetry=np.pi/2, exec_mode='auto', interpolate=False)

        # doing cycle slip correction, assuming coarse cpe is immune to cycle slips
        est_phase_noise_fine_after_cs_correction = cycle_slip_correction(cpe_coarse_results['est_phase_noise_coarse'], -cpe_fine_results['est_phase_noise'], quantize_value=np.pi/2, jump_point_avag=None, debug=False)

        # writing back samples after correction
        # first, we have to normalise the samples
        mag_const = np.mean(abs(sig_rx.constellation[dim]))
        mag_samples = np.mean(abs(sig_rx.samples[dim]))
        sig_rx.samples[dim] = sig_rx.samples[dim] * mag_const / mag_samples
        # second, writing back after cycle slip correction, with len given from cycle slip correction
        sig_rx.samples[dim] = sig_rx.samples[dim][:len(est_phase_noise_fine_after_cs_correction)] * np.exp(-1j * est_phase_noise_fine_after_cs_correction)
        
        # cut all arrays to len of fine after cs correction. Due to cropping in steps before
        pilot_array = pilot_array[:len(est_phase_noise_fine_after_cs_correction)]
        cpe_coarse_results['est_phase_noise_coarse'] = cpe_coarse_results['est_phase_noise_coarse'][:len(est_phase_noise_fine_after_cs_correction)]
        cpe_fine_results['est_phase_noise'] = cpe_fine_results['est_phase_noise'][:len(est_phase_noise_fine_after_cs_correction)]
        est_phase_noise_fine_after_cs_correction = est_phase_noise_fine_after_cs_correction[:len(est_phase_noise_fine_after_cs_correction)]

        # remove CPE pilots
        sig_rx.samples[dim] = sig_rx.samples[dim][pilot_array==0.0]
        cpe_coarse_results['est_phase_noise_coarse'] = cpe_coarse_results['est_phase_noise_coarse'][pilot_array==0.0]
        cpe_fine_results['est_phase_noise'] = cpe_fine_results['est_phase_noise'][pilot_array==0.0]
        est_phase_noise_fine_after_cs_correction = est_phase_noise_fine_after_cs_correction[pilot_array==0.0]

        # build up results dict
        cpe_results_dict[dim]["est_phase_noise_coarse"] = cpe_coarse_results['est_phase_noise_coarse']
        cpe_results_dict[dim]["est_phase_noise_fine_before_cs_correction"] = -cpe_fine_results['est_phase_noise']
        cpe_results_dict[dim]["est_phase_noise_fine_after_cs_correction"] = est_phase_noise_fine_after_cs_correction

    if debug is True:
        # generate debug figure, based on subplots for each dim
        fig = plt.figure(fNum)
        ax_overall_list = []
        idx = 1
        for dim in range(sig_rx.n_dims):
            ax_overall_list.append(fig.add_subplot(2, sig_rx.n_dims, idx))
            ax_overall_list[-1].set_title('Dim {}'.format(dim))
            ax_overall_list[-1].plot(cpe_results_dict[dim]["est_phase_noise_coarse"],label='coarse PE')
            ax_overall_list[-1].plot(cpe_results_dict[dim]["est_phase_noise_fine_before_cs_correction"],label='fine PE')
            ax_overall_list[-1].plot(cpe_results_dict[dim]["est_phase_noise_fine_after_cs_correction"],label='fine PE after CS correction')
            for frame in np.arange(n_dsp_frames):
                ax_overall_list[-1].axvline(sig_rx.symbols[dim].size*frame,ls='--',color='r')

            idx += 1
            # Geenie-based phase noise estimation
            ratio_base = int(sig_rx.samples[dim].size // (sig_rx.symbols[dim].size))
            ratio_rem = int(sig_rx.samples[dim].size % (sig_rx.symbols[dim].size))
            symbols = np.concatenate((np.tile(sig_rx.symbols[dim], ratio_base), sig_rx.symbols[dim][:ratio_rem]), axis=0)
    
            phasenoise_geenie = np.unwrap(np.angle(sig_rx.samples[dim] * np.conj(symbols)))

            ax_overall_list.append(fig.add_subplot(2, sig_rx.n_dims, idx))
            ax_overall_list[-1].plot(phasenoise_geenie, label="(rest)-phasenoise")
            ax_overall_list[-1].plot(cpe_results_dict[dim]["est_phase_noise_fine_after_cs_correction"]+cpe_results_dict[dim]["est_phase_noise_coarse"], label="estimated phase in total")    
            for frame in np.arange(n_dsp_frames):
                ax_overall_list[-1].axvline(sig_rx.symbols[dim].size*frame,ls='--',color='r')
            ax_overall_list[-1].set_title("Dim {}: estimated phase random walk".format(dim))

            idx += 1
        
        for ax in ax_overall_list:
            ax.legend()
            ax.grid(which="both")
            ax.set_xlabel("time / symbols")
            ax.set_ylabel("phase (rad)") 

        plt.tight_layout()
    
    # return results
    return sig_rx, cpe_results_dict