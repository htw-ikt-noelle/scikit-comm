import unittest
import numpy as np
import scipy.signal as ssignal
from numpy.testing import (
        assert_, assert_raises, assert_equal, assert_warns, assert_almost_equal,
        assert_no_warnings, assert_array_equal, assert_array_almost_equal,
        assert_allclose, suppress_warnings
        )

from .. import tx
from .. import rx
from .. import utils
from .. import signal
from .. import channel
from .. import visualizer


class TestDemapper(unittest.TestCase):
    """
    Test class for demapper
    """
    
    def test_four_point_const(self):
        bits = np.asarray([0,0,0,1,1,1,1,0]) # 0 1 3 2
        samples = np.asarray([1+0j, 0+1j, 0-1j, -1+0j])
        constellation = np.asarray([1+0j, 0+1j, -1+0j, 0-1j])
        demapped = rx.demapper(samples, constellation)
        assert_equal(demapped, bits)
        
class TestDecision(unittest.TestCase):
    """
    Test class for decision
    """
    
    def test_four_point_const(self):
        samples = np.asarray([2+0j, 1+0.99j, 0.01-0j, 0.5-0.49j])
        constellation = np.asarray([1+0j, 0+1j, -1+0j, 0-1j])
        result = np.asarray([1+0j, 1+0j, 1+0j, 1+0j]) 
        dec = rx.decision(samples, constellation)
        assert_equal(result, dec)
        
    def test_scaling(self):
        constellation = np.asarray([1,2,3,4])
        samples1 = constellation*4
        samples2 = constellation*0.1
        result = constellation
        dec1 = rx.decision(samples1, constellation)
        dec2 = rx.decision(samples2, constellation)
        assert_equal(result, dec1)
        assert_equal(result, dec2)
        
class TestSamplingPhaseClockAdjustment(unittest.TestCase):
    """
    Test class for sampling_phase_adjustment and sampling_clock_adjumstment
    """
    
    def test_four_point_const(self):
        sr = 10.0
        symbr = 1.0
        phase = 1.0
        n_samples = 100
        t = utils.create_time_axis(sample_rate=10.0, n_samples=n_samples)
        samples = np.cos(2*np.pi*symbr/2*t + phase)
        shift = rx.sampling_phase_adjustment(samples, sample_rate=sr, symbol_rate=symbr, shift_dir='delay')['est_shift']
        assert_array_almost_equal(shift, -phase/np.pi/symbr, decimal=10)
        shift = rx.sampling_phase_adjustment(samples, sample_rate=sr, symbol_rate=symbr, shift_dir='advance')['est_shift']
        assert_array_almost_equal(shift, 1.0-(phase/np.pi/symbr), decimal=10)
        shift = rx.sampling_phase_adjustment(samples, sample_rate=sr, symbol_rate=symbr, shift_dir='both')['est_shift']
        assert_array_almost_equal(shift, -phase/np.pi/symbr, decimal=10)
        shifts = rx.sampling_clock_adjustment(samples, sample_rate=sr, symbol_rate=symbr, block_size=int(n_samples/(sr/symbr*2)))['est_shift']
        assert_array_almost_equal(shifts, np.asarray([-phase/np.pi/symbr, -phase/np.pi/symbr]), decimal=10)
        

class TestSymbolSequenceSync(unittest.TestCase):
    """
    Test class for symbol_sequence_sync
    """
    
    def test_delays_and_phase_shifts(self):
        # generate test signal
        sig = signal.Signal(n_dims=2)
        sig.constellation = [np.asarray([-1.0, 1.0]), np.asarray([-1.0-1.0j,-1.0+1.0j, 1.0-1.0j, 1.0+1.0j])]
        sig.symbols = [sig.constellation[0][np.random.randint(low=0,high=2,size=100)], sig.constellation[1][np.random.randint(low=0,high=4,size=100)]]
        # 1) no shift, no phase error
        sig.samples = sig.symbols
        tmp = rx.symbol_sequence_sync(sig, dimension=-1)
        tmp[0]['phase_est'] = np.abs(tmp[0]['phase_est'])
        tmp[1]['phase_est'] = np.abs(tmp[1]['phase_est'])          
        assert_equal(tmp[0],
                {'symbol_delay_est':0, 'phase_est': 0.0}
                )
        assert_equal(tmp[1],
                {'symbol_delay_est':0, 'phase_est': 0.0}
                )
        # 2) shift, shift+phase error
        sig.samples = [np.roll(sig.symbols[0],shift=-10), np.roll(sig.symbols[1], shift=-5)*np.exp(1j*np.pi)]
        tmp = rx.symbol_sequence_sync(sig, dimension=-1)        
        assert_equal(tmp[0],
                {'symbol_delay_est':-10, 'phase_est':0.0}
                )
        assert_equal(tmp[1],
                {'symbol_delay_est':-5, 'phase_est':-np.pi}
                )
        # 3) shift+phase error, shift+conj+phase phase error
        sig.samples = [np.roll(sig.symbols[0]*np.exp(-1j*np.pi/2),shift=-10), np.conj(np.roll(sig.symbols[1], shift=-10)*np.exp(1j*np.pi))]
        tmp = rx.symbol_sequence_sync(sig, dimension=-1)        
        assert_equal(tmp[0],
                {'symbol_delay_est':-10, 'phase_est':np.pi/2}
                )
        assert_equal(tmp[1],
                {'symbol_delay_est':-10, 'phase_est':np.pi}
                )

class TestFrequencyOffsetCorrection(unittest.TestCase):
    """
    Test class for frequency offset correction
    """

    def test_standard_FOC(self):
        # generate test signal
        sig = signal.Signal(n_dims=1)
        sig.constellation = [np.asarray([-1-1j,-1+1j, 1-1j, 1+1j])]
        sig.symbols = [sig.constellation[0][np.random.randint(low=0,high=2,size=int(1e5))]]
        sig.symbol_rate = 10e9
        sig.pulseshaper(upsampling=2, pulseshape='rrc', roll_off=[0.1])
        # 1) no frequency offset
        tmp_samples = sig.samples[0]
        tmp = rx.frequency_offset_correction(tmp_samples, sample_rate=sig.sample_rate[0], symbol_rate=sig.symbol_rate[0])
        assert_allclose(tmp['estimated_fo'], 0, rtol=1e-5, atol=20)
        # 2) frequency offset
        t = (np.arange(0, np.size(sig.samples[0])) / sig.sample_rate[0])
        tmp_samples = sig.samples[0]*np.exp(1j*2*np.pi*100e6*t)  
        tmp = rx.frequency_offset_correction(tmp_samples, sample_rate=sig.sample_rate[0], symbol_rate=sig.symbol_rate[0])
        assert_allclose(tmp['estimated_fo'], 100e6, rtol=1e-5, atol=0)

class TestStokesEQ(unittest.TestCase):
    """
    Test class for rx.stokesEQ_pdm():
        test Stokes-based polarization equalization
    """
    def test_stokesEQ_pdmQpsk(self):
        """
        PDM-QPSK: check, if signal lies solely in S2-S3 plane of Stokes space after equalization
        """
        # create test data (noise-free PDM-QPSK symbols, 1 sample per symbol)
        N_symbols = int(1e4) # number of symbols per polarization

        # 4x4=16 4D-QPSK constellations
        Constellations = (utils.dec_to_bits(range(16),4).reshape((16,4)).astype(int)*2-1).astype(float)

        # Tx: randomly pick (equally distributed in 4D) PDM-QPSK symbols
        rng = np.random.default_rng(seed=1)
        symbols_4D = Constellations[rng.permutation(N_symbols)%Constellations.shape[0]]
        sig_X = symbols_4D[:,:2].view(np.complex128).flatten() # *np.exp(1j*np.pi/4)
        sig_Y = symbols_4D[:,2:].view(np.complex128).flatten()

        upsample = 2
        if upsample>1: # upsampling to 2SpS raised-cosine pulseshape with rolloff=0.1 (requires Rtol = Atol = 1e-3 )
                sig_X = tx.pulseshaper(sig_X, upsampling=upsample, pulseshape="rc", roll_off=0.1)
                sig_Y = tx.pulseshaper(sig_Y, upsampling=upsample, pulseshape="rc", roll_off=0.1)
        
        # measure Stokes parameters of Tx (signal SOPs are in S2-S3 plane)
        result_j2s_Tx = utils.jones2stokes_pdm(sig_X, sig_Y)

        # rotate this constellation first around S2-axis by pi/4 (45°) 
        result_rp = channel.rotatePol_pdm(sig_X, sig_Y, theta=0.5*np.pi/4, psi=0.0, phi=np.pi/2)
        # next, rotate around S3-axis by pi/4 (45°) 
        result_rp = channel.rotatePol_pdm(result_rp["samples_X"], result_rp["samples_Y"], theta=0.5*np.pi/4, psi=0.0, phi=0.0)

        # visualizer.plot_poincare_sphere(result_rp["samples_X"],result_rp["samples_Y"],tit='before EQ', decimation=upsample, fNum=1)

        # equalize
        result_eq = rx.stokesEQ_pdm(result_rp["samples_X"],result_rp["samples_Y"])

        # visualizer.plot_poincare_sphere(result_eq["samples_X"],result_eq["samples_Y"],tit='before EQ', decimation=upsample, fNum=2)

        # get Stokes parameters after Stokes-equalizer
        result_j2s_Eq = utils.jones2stokes_pdm(result_eq["samples_X"], result_eq["samples_Y"])

        # test if Stokes parameters after pol-rotation and Stokes equalization equal the original Stokes parameters after Tx
        Rtol = 1e-15
        Atol = 1e-3

        assert_allclose((result_j2s_Tx["S0"][::upsample].__abs__()**2).mean(), (result_j2s_Eq["S0"][::upsample].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
        assert_allclose((result_j2s_Tx["S1"][::upsample].__abs__()**2).mean(), (result_j2s_Eq["S1"][::upsample].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
        assert_allclose((result_j2s_Tx["S2"][::upsample].__abs__()**2).mean(), (result_j2s_Eq["S2"][::upsample].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
        assert_allclose((result_j2s_Tx["S3"][::upsample].__abs__()**2).mean(), (result_j2s_Eq["S3"][::upsample].__abs__()**2).mean(), rtol=Rtol, atol=Atol)

#     def test_stokesEQ_pdmBpsk(self):
#         """
#         PDM-BPSK: check, if signal lies solely on S2-axis of Stokes space after equalization
#         """
#         # create test data (noise-free PDM-BPSK symbols, 1 sample per symbol)
#         N_symbols = 1000 # number of symbols per polarization

#         # 2x2=4 4D-BPSK constellations
#         Constellations = (utils.dec_to_bits(range(4),2).reshape((4,2)).astype(int)*2-1).astype(float)

#         # Tx: randomly pick (equally distributed in 4D) PDM-BPSK symbols
#         rng = np.random.default_rng(seed=1)
#         symbols_4D = Constellations[rng.permutation(N_symbols)%Constellations.shape[0]]
#         sig_X = symbols_4D[:,0].flatten() # * np.exp(1j*np.pi/4)
#         sig_Y = symbols_4D[:,1].flatten()

#         if True: # upsampling to 2SpS raised-cosine p<ulseshape with rolloff=0.1
#                 sig_X = tx.pulseshaper(sig_X, upsampling=2, pulseshape="rc", roll_off=0.1)
#                 sig_Y = tx.pulseshaper(sig_Y, upsampling=2, pulseshape="rc", roll_off=0.1)

#         # measure Stokes parameters of Tx (signal SOPs are in S2-S3 plane)
#         result_j2s_Tx = utils.jones2stokes_pdm(sig_X, sig_Y)

#         # first, rotate this constellation around S3-axis by pi/4 (45°) 
#         result_rp = channel.rotatePol_pdm(sig_X, sig_Y, theta=0.5*np.pi/4, psi=0.0, phi=0.0)
#         # next, rotate around S2-axis by pi/4 (45°) 
#         result_rp = channel.rotatePol_pdm(result_rp["samples_X"], result_rp["samples_Y"], theta=0.5*np.pi/4, psi=0.0, phi=np.pi/2)

#         # # measure Stokes parameters before Eq
#         # result_j2s_Rx = utils.jones2stokes_pdm(result_rp["samples_X"],result_rp["samples_Y"])

#         # equalize
#         result_eq = rx.stokesEQ_pdm(result_rp["samples_X"],result_rp["samples_Y"])

#         # measure Stokes parameters before Eq
#         result_j2s_Eq = utils.jones2stokes_pdm(result_eq["samples_X"], result_eq["samples_Y"])

#         # test if Stokes parameters after pol-rotation and Stokes equalization equal the original Stokes parameters after Tx
#         Rtol = 1e-15
#         Atol = 1e-15
#         assert_allclose((result_j2s_Tx["S0"].__abs__()**2).mean(), (result_j2s_Eq["S0"].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
#         assert_allclose((result_j2s_Tx["S1"].__abs__()**2).mean(), (result_j2s_Eq["S1"].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
#         assert_allclose((result_j2s_Tx["S2"].__abs__()**2).mean(), (result_j2s_Eq["S2"].__abs__()**2).mean(), rtol=Rtol, atol=Atol)
#         assert_allclose((result_j2s_Tx["S3"].__abs__()**2).mean(), (result_j2s_Eq["S3"].__abs__()**2).mean(), rtol=Rtol, atol=Atol)

class TestPilotBasedCpe(unittest.TestCase):
    """
    Test class for testing pilot based CPE
    """

    def test_pilot_based_cpe(self):
        # test signal
        samples_orig = np.tile(np.asarray([-1-1j,-1+1j, 1-1j, 1+1j]), 100)
        samples_with_pn = samples_orig*np.exp(-1j*np.pi/5)
        pilot_array = np.tile(np.asarray([0,0, 0, 1+1j]), 100)
        # test the function
        res = rx.pilot_aided_coarse_cpe(samples_with_pn, pilot_array, M=4, N=1, debug=False)
        assert_array_equal(samples_orig[res["coarse_cpe_filter_len"]:-res["coarse_cpe_filter_len"]], res["samples_corrected"][res["coarse_cpe_filter_len"]:-res["coarse_cpe_filter_len"]])

class TestCycleSlipCorrection(unittest.TestCase):
    """
    Test class for cycle slip correction
    """

    def test_cycle_slip_correction(self):
        # test signal
        coarse_phase_est = np.array([np.pi/4, np.pi/2, np.pi, np.pi*1.5])
        fine_phase_est = np.array([0, np.pi/5, -np.pi+0.1, 0])
        # test function - function should be take care of -np.pi and respond 0.1 on third position
        tmp = rx.cycle_slip_correction(coarse_phase_est, fine_phase_est, quantize_value=np.pi/2, jump_point_avag=None, debug=False)
        assert_array_almost_equal(tmp, np.array([0,np.pi/5,0.1,0]))

class DaFrameSync(unittest.TestCase):
    """
    Test class for DA Frame Synchronization
    """

    def test_correct_shift(self):
        # generate random input samples
        N = 64
        delay = 35
        header = np.random.randn(N) +1j*np.random.randn(N)
        header = np.concatenate((header, header, -header, header))
        data = np.random.randn(1000) +1j*np.random.randn(1000)
        samples = np.concatenate((header,data))
        samples = np.roll(samples, delay)
        est_shift_p = rx.da_frame_sync(samples.copy(), shift=N, search_range=500, dbg_plot=False, fNum=1, exec_mode='python')
        # test for equality
        assert_equal(est_shift_p, delay)

class DaCFOEstimation(unittest.TestCase):
    """
    Test class for DA CFO estimation
    """

    def test_correct_cfo(self):
        # generate random input samples
        N = 64
        sample_rate = 32e9
        cfo = 1e6
        header = np.random.randn(N) +1j*np.random.randn(N)
        header = np.concatenate((header, header, -header, header))
        data = np.random.randn(1000) +1j*np.random.randn(1000)
        samples = np.concatenate((header,data))
        t = np.arange(0,samples.size)/sample_rate
        samples *= np.exp(1j*2*np.pi*cfo*t)
        est_cfo = rx.da_cfo_estimation(samples, shift=N, sample_rate=sample_rate)
        # test for equality
        assert_almost_equal(est_cfo,cfo,decimal=6)

class TestChannelEstimation(unittest.TestCase):
    """
    Test class for channel estimation
    """

    def test_channel_estimation(self):
        # build up rng
        rng = np.random.default_rng(seed=1234)
        # build up test signal
        n_fft = 64
        n_pilots = n_fft
        oversampling = 2
        alphabet = np.array([1+1j, -1+1j, -1-1j, 1-1j])
        pilot_pos = np.sort(np.mod(np.arange(n_fft,step=n_fft/n_pilots,dtype=int),n_fft))
        mean_power = np.mean(np.abs(alphabet)**2)
        pilots = np.sqrt(mean_power) * 1/np.sqrt(2) * n_fft / np.sqrt(n_pilots) * rng.choice(alphabet, size=n_pilots)
        # build up ce header
        ce_header = np.fft.ifft(pilots)
        # do a resampling to 2 sps
        ce_header = ssignal.resample(ce_header, int(oversampling *len(ce_header)))
        # converting ce header to spectral domain
        ce_header_spec = np.fft.fft(ce_header)
        ce_header_spec /= oversampling
        # calculate channel
        H_est = rx.da_channel_estimation(ce_header_rx_spec=ce_header_spec, 
                                                        pilots=pilots,
                                                        pilot_pos=pilot_pos, 
                                                        n_fft=n_fft)
        # channel amp and phase should be zero 
        array_slice = H_est[33:-33]
        assert_allclose(np.abs(array_slice), 0, atol=1e-10)