# This file may be used to install all dependencies to generate the documentation of scikit-comm package using pip with the following command
# pip install -r requirements.txt 
# generated and tested with python version 3.11.2
matplotlib>=3.8.4
myst-parser>=4.0.0
numpy>=2.0.2
PyVISA>=1.14.1
PyVISA-py>=0.7.2
scipy>=1.14.1
Sphinx>=8.1.3
screeninfo>=0.8.1
sphinx-rtd-theme>=3.0.1
sphinx-autodoc-typehints>=2.5.0
nbsphinx>=0.9.5
nbsphinx-link>=1.3.1