.. skcomm documentation master file, created by
   sphinx-quickstart on Tue Jun 16 12:53:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the scikit-comm documentation
========================================

.. toctree::
   :maxdepth: 1
   :caption: Getting Started
   
   gettingStarted   

.. toctree::
   :maxdepth: 1
   :caption: Examples

   sim_baseband_minimal   
   sim_baseband
   sim_homodyne
   sim_if_transmission
   experiment_homodyne
   experiment_heterodyne
   sim_data_aided_homodyne_PDM

.. toctree::
   :maxdepth: 1
   :caption: API Documentation

   api_doc
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`