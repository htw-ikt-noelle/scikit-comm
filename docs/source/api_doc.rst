Channel Submodule (:mod:`skcomm.channel`)
-----------------------------------------
This submodule includes routines to emulate a transmission channel.

.. automodule:: skcomm.channel
    :members:
        

Filter Submodule (:mod:`skcomm.filters`)
----------------------------------------
This submodule includes filtering routines.

.. automodule:: skcomm.filters
     :members:
    

Instrument Control Submodule (:mod:`skcomm.instrument_control`)
---------------------------------------------------------------
This submodule includes routines to communicate to laboratory equipment.

.. automodule:: skcomm.instrument_control
    :members:
       

Pre Distortion Submodule (:mod:`skcomm.pre_distortion`)
-------------------------------------------------------
This submodule includes routines to linearly pre-distort systems.

.. automodule:: skcomm.pre_distortion
    :members:
     

Receiver Submodule (:mod:`skcomm.rx`)
-------------------------------------
This submodule includes receiver routines.

.. automodule:: skcomm.rx
    :members:
    

Signal Submodule (:mod:`skcomm.signal`)
---------------------------------------
This submodule includes the basic Signal class.

.. automodule:: skcomm.signal
    :members:


Transmitter Submodule (:mod:`skcomm.tx`)
----------------------------------------
This submodule includes transmitter routines.

.. automodule:: skcomm.tx
    :members:
 

Utilities Submodule (:mod:`skcomm.utils`)
-----------------------------------------
This submodule includes utility routines.

.. automodule:: skcomm.utils
    :members:
  

Visulizer Submodule (:mod:`skcomm.visualizer`)
----------------------------------------------
This submodule includes routines to visualize signal.

.. automodule:: skcomm.visualizer
    :members:
  

